/*
 * Copyright (c) 2005 inswave, Inc. All Rights Reserved.
 *
 */
package com.ews.sta.xda;

import com.inswave.system.xda.*;
/**
 * com.ews.sta.xda.StatInfo005_147
 */
public class StatInfo005_147 extends AccessInfo {
	public StatInfo005_147() {
		type = Constants.SELECT;
		defaultQuery = "SELECT \n        \"시나리오ID\"\n      , \"시나리오명\"\n      , \"시나리오등급\"\n      , \"모니터링주기\" \n      , \"회계년도\"\n      , \"전표번호\"        \n      , \"전표라인\"        \n      , \"전표유형\"        \n      , \"전표유형명\"        \n      , \"전기일\"        \n      , \"증빙일\"        \n      , \"차이일수\"  \n      , \"금액\"        \n      , \"부서명\"   \n      , \"생성자명\"   \n      , \"생성자ID\"\n      , \"EWS가동일\"\n      , \"실무상태\"   \n      , \"실무승인자\"\n      , \"실무승인자명\"   \n      , \"주관상태\"   \n      , \"주관승인자\"\n      , \"주관승인자명\"\n      , \"[실무]점검문서번호\"       \n      , \"[실무]점검제목\"    \n      , \"[실무]점검문서작성일\"     \n      , \"[실무]점검부서\"\n      , \"[실무]점검부서명\"     \n      , \"[실무]점검자\"\n      , \"[실무]점검자명\"      \n      , \"[실무]발생원인\"    \n      , \"[실무]처리방안\"     \n      , \"[실무]진행상태\"   \n      , \"[실무]승인자\"\n      , \"[실무]승인자명\"     \n      , \"[실무]승인일자\"   \n      , \"[실무]승인자코멘트\"\n      , \"[주관]점검문서번호\"       \n      , \"[주관]점검제목\"    \n      , \"[주관]점검문서작성일\"     \n      , \"[주관]점검부서\"\n      , \"[주관]점검부서명\"     \n      , \"[주관]점검자\"\n      , \"[주관]점검자명\"      \n      , \"[주관]발생원인\"    \n      , \"[주관]처리방안\"     \n      , \"[주관]진행상태\"   \n      , \"[주관]승인자\"   \n      , \"[주관]승인자명\"  \n      , \"[주관]승인일자\"   \n      , \"[주관]승인자코멘트\"   \nFROM EWS_SP147_V\nWHERE \"EWS가동일\" BETWEEN ? AND ?";
		sybaseQuery = null;
		oracleQuery = null;
		mssqlQuery = null;
		mysqlQuery = null;
		db2Query = null;
		informixQuery = null;

		dynamicQuery = new String[] {
			
		};

		paramData = new String[] {
			"ST_DT", "ED_DT"
		};
		paramDataType = new int[] { Constants.STRING, Constants.STRING };
		paramMode = new int[] {Constants.IN, Constants.IN};

		returnMSG = "RESULT";
		returnType = Constants.VECTOR;
		returnData = new String[][] {
			{"시나리오ID", "시나리오ID"},
			{"시나리오명", "시나리오명"},
			{"시나리오등급", "시나리오등급"},
			{"모니터링주기", "모니터링주기"},
			{"회계년도", "회계년도"},
			{"전표번호", "전표번호"},
			{"전표라인", "전표라인"},
			{"전표유형", "전표유형"},
			{"전표유형명", "전표유형명"},
			{"전기일", "전기일"},
			{"증빙일", "증빙일"},
			{"차이일수", "차이일수"},
			{"금액", "금액"},
			{"부서명", "부서명"},
			{"생성자명", "생성자명"},
			{"생성자ID", "생성자ID"},
			{"EWS가동일", "EWS가동일"},
			{"실무상태", "실무상태"},
			{"실무승인자", "실무승인자"},
			{"실무승인자명", "실무승인자명"},
			{"주관상태", "주관상태"},
			{"주관승인자", "주관승인자"},
			{"주관승인자명", "주관승인자명"},
			{"[실무]점검문서번호", "l실무l점검문서번호"},
			{"[실무]점검제목", "l실무l점검제목"},
			{"[실무]점검문서작성일", "l실무l점검문서작성일"},
			{"[실무]점검부서", "l실무l점검부서"},
			{"[실무]점검부서명", "l실무l점검부서명"},
			{"[실무]점검자", "l실무l점검자"},
			{"[실무]점검자명", "l실무l점검자명"},
			{"[실무]발생원인", "l실무l발생원인"},
			{"[실무]처리방안", "l실무l처리방안"},
			{"[실무]진행상태", "l실무l진행상태"},
			{"[실무]승인자", "l실무l승인자"},
			{"[실무]승인자명", "l실무l승인자명"},
			{"[실무]승인일자", "l실무l승인일자"},
			{"[실무]승인자코멘트", "l실무l승인자코멘트"},
			{"[주관]점검문서번호", "l주관l점검문서번호"},
			{"[주관]점검제목", "l주관l점검제목"},
			{"[주관]점검문서작성일", "l주관l점검문서작성일"},
			{"[주관]점검부서", "l주관l점검부서"},
			{"[주관]점검부서명", "l주관l점검부서명"},
			{"[주관]점검자", "l주관l점검자"},
			{"[주관]점검자명", "l주관l점검자명"},
			{"[주관]발생원인", "l주관l발생원인"},
			{"[주관]처리방안", "l주관l처리방안"},
			{"[주관]진행상태", "l주관l진행상태"},
			{"[주관]승인자", "l주관l승인자"},
			{"[주관]승인자명", "l주관l승인자명"},
			{"[주관]승인일자", "l주관l승인일자"},
			{"[주관]승인자코멘트", "l주관l승인자코멘트"}
		};
		returnDataType = new int[] { Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING };
	}
}
