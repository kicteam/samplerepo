/*
 * Copyright (c) 2005 inswave, Inc. All Rights Reserved.
 *
 */
package com.ews.sta.xda;

import com.inswave.system.xda.*;
/**
 * com.ews.sta.xda.StatInfo005_409
 */
public class StatInfo005_409 extends AccessInfo {
	public StatInfo005_409() {
		type = Constants.SELECT;
		defaultQuery = "SELECT \n        \"시나리오ID\"\n      , \"시나리오명\"\n      , \"시나리오등급\"\n      , \"모니터링주기\" \n\n      , \"요청일자\"\n      , \"자재코드\"\n      , \"자재내역\"\n      , \"단위\"\n      , \"보유수량\"\n      , \"출고예정l사용예약l\"\n      , \"OPENPR\"\n      , \"OPENPO\"\n      , \"입고예정l기술검수l\"\n      , \"잔존자재여부\"\n      , \"추가요청자재l추가PR수량l\"\n      , \"자재과부족l가용자재l\"\n      , \"예약번호\"\n      , \"예약품번\"\n      , \"사원번호\"\n      , \"부서명\"\n      , \"PR번호\"\n      , \"PR품번\"\n      , \"추가PR금액\"\n      , \"PO번호\"\n      , \"PO품번\"\n      , \"인가오픈P/R\"\n      , \"예약참조오픈PR\"\n      , \"직접입력긴급오픈PR\"\n      , \"기타오픈PR\"\n      , \"총PO\"\n      , \"WESBS_103_QTY\"\n      , \"WESBS_104_QTY\"\n      , \"WESBS_105_QTY\"\n      , \"WESBS_106_QTY\"\n      , \"EWS가동일\"\n      , \"실무상태\"   \n      , \"실무승인자\"\n      , \"실무승인자명\"   \n      , \"주관상태\"   \n      , \"주관승인자\"\n      , \"주관승인자명\"\n      ,\"[실무]점검문서번호\" AS  \"l실무l점검문서번호\"\n      ,\"[실무]점검제목\" AS \"l실무l점검제목\"\n      ,\"[실무]점검문서작성일\" AS \"l실무l점검문서작성일\" \n      ,\"[실무]점검부서\" AS \"l실무l점검부서\"\n      ,\"[실무]점검부서명\" AS \"l실무l점검부서명\"     \n      ,\"[실무]점검자\" AS \"l실무l점검자\"\n      ,\"[실무]점검자명\" AS \"l실무l점검자명\"      \n      ,\"[실무]발생원인\" AS \"l실무l발생원인\"    \n      ,\"[실무]처리방안\" AS \"l실무l처리방안\"     \n      ,\"[실무]진행상태\" AS \"l실무l진행상태\"\n      ,\"[실무]승인자\" AS \"l실무l승인자\"\n      ,\"[실무]승인자명\" AS \"l실무l승인자명\"    \n      ,\"[실무]승인일자\" AS \"l실무l승인일자\"   \n      ,\"[실무]승인자코멘트\" AS \"l실무l승인자코멘트\"\n      ,\"[주관]점검문서번호\" AS \"l주관l점검문서번호\"       \n      ,\"[주관]점검제목\" AS \"l주관l점검제목\"    \n      ,\"[주관]점검문서작성일\" AS \"l주관l점검문서작성일\"     \n      ,\"[주관]점검부서\" AS \"l주관l점검부서\"\n      ,\"[주관]점검부서명\" AS \"l주관l점검부서명\"     \n      ,\"[주관]점검자\" AS \"l주관l점검자\" \n      ,\"[주관]점검자명\" AS \"l주관l점검자명\"      \n      ,\"[주관]발생원인\" AS \"l주관l발생원인\"   \n      ,\"[주관]처리방안\" AS \"l주관l처리방안\"\n      ,\"[주관]진행상태\" AS \"l주관l진행상태\"  \n      ,\"[주관]승인자\" AS \"l주관l승인자\" \n      ,\"[주관]승인자명\" AS \"l주관l승인자명\"  \n      ,\"[주관]승인일자\" AS \"l주관l승인일자\"   \n      ,\"[주관]승인자코멘트\" AS \"l주관l승인자코멘트\"\nFROM EWS_SP409_V\nWHERE \"EWS가동일\" BETWEEN ? AND ?";
		sybaseQuery = null;
		oracleQuery = null;
		mssqlQuery = null;
		mysqlQuery = null;
		db2Query = null;
		informixQuery = null;

		dynamicQuery = new String[] {
			
		};

		paramData = new String[] {
			"ST_DT", "ED_DT"
		};
		paramDataType = new int[] { Constants.STRING, Constants.STRING };
		paramMode = new int[] {Constants.IN, Constants.IN};

		returnMSG = "RESULT";
		returnType = Constants.VECTOR;
		returnData = new String[][] {
			{"시나리오ID", "시나리오ID"},
			{"시나리오명", "시나리오명"},
			{"시나리오등급", "시나리오등급"},
			{"모니터링주기", "모니터링주기"},
			{"요청일자", "요청일자"},
			{"자재코드", "자재코드"},
			{"자재내역", "자재내역"},
			{"단위", "단위"},
			{"보유수량", "보유수량"},
			{"출고예정l사용예약l", "출고예정l사용예약l"},
			{"OPENPR", "OPENPR"},
			{"OPENPO", "OPENPO"},
			{"입고예정l기술검수l", "입고예정l기술검수l"},
			{"잔존자재여부", "잔존자재여부"},
			{"추가요청자재l추가PR수량l", "추가요청자재l추가PR수량l"},
			{"자재과부족l가용자재l", "자재과부족l가용자재l"},
			{"예약번호", "예약번호"},
			{"예약품번", "예약품번"},
			{"사원번호", "사원번호"},
			{"부서명", "부서명"},
			{"PR번호", "PR번호"},
			{"PR품번", "PR품번"},
			{"추가PR금액", "추가PR금액"},
			{"PO번호", "PO번호"},
			{"PO품번", "PO품번"},
			{"인가오픈P/R", "인가오픈P/R"},
			{"예약참조오픈PR", "예약참조오픈PR"},
			{"직접입력긴급오픈PR", "직접입력긴급오픈PR"},
			{"기타오픈PR", "기타오픈PR"},
			{"총PO", "총PO"},
			{"WESBS_103_QTY", "WESBS_103_QTY"},
			{"WESBS_104_QTY", "WESBS_104_QTY"},
			{"WESBS_105_QTY", "WESBS_105_QTY"},
			{"WESBS_106_QTY", "WESBS_106_QTY"},
			{"EWS가동일", "EWS가동일"},
			{"실무상태", "실무상태"},
			{"실무승인자", "실무승인자"},
			{"실무승인자명", "실무승인자명"},
			{"주관상태", "주관상태"},
			{"주관승인자", "주관승인자"},
			{"주관승인자명", "주관승인자명"},
			{"l실무l점검문서번호", "l실무l점검문서번호"},
			{"l실무l점검제목", "l실무l점검제목"},
			{"l실무l점검문서작성일", "l실무l점검문서작성일"},
			{"l실무l점검부서", "l실무l점검부서"},
			{"l실무l점검부서명", "l실무l점검부서명"},
			{"l실무l점검자", "l실무l점검자"},
			{"l실무l점검자명", "l실무l점검자명"},
			{"l실무l발생원인", "l실무l발생원인"},
			{"l실무l처리방안", "l실무l처리방안"},
			{"l실무l진행상태", "l실무l진행상태"},
			{"l실무l승인자", "l실무l승인자"},
			{"l실무l승인자명", "l실무l승인자명"},
			{"l실무l승인일자", "l실무l승인일자"},
			{"l실무l승인자코멘트", "l실무l승인자코멘트"},
			{"l주관l점검문서번호", "l주관l점검문서번호"},
			{"l주관l점검제목", "l주관l점검제목"},
			{"l주관l점검문서작성일", "l주관l점검문서작성일"},
			{"l주관l점검부서", "l주관l점검부서"},
			{"l주관l점검부서명", "l주관l점검부서명"},
			{"l주관l점검자", "l주관l점검자"},
			{"l주관l점검자명", "l주관l점검자명"},
			{"l주관l발생원인", "l주관l발생원인"},
			{"l주관l처리방안", "l주관l처리방안"},
			{"l주관l진행상태", "l주관l진행상태"},
			{"l주관l승인자", "l주관l승인자"},
			{"l주관l승인자명", "l주관l승인자명"},
			{"l주관l승인일자", "l주관l승인일자"},
			{"l주관l승인자코멘트", "l주관l승인자코멘트"}
		};
		returnDataType = new int[] { Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING };
	}
}
