/*
 * Copyright (c) 2005 inswave, Inc. All Rights Reserved.
 *
 */
package com.ews.sta.xda;

import com.inswave.system.xda.*;
/**
 * com.ews.ope.xda.Opermng019_002
 */
public class StatInfo006_002 extends AccessInfo {
	public StatInfo006_002() {
		type = Constants.INSERT;
		defaultQuery = "INSERT INTO EWS_BOARD\n" +
				"(USE_YN,REF_ID, SEQ, TITLE, CONTENT, CR_EMPID, CR_NAME, FL_ID, DASH_USE)\n" +
				"VALUES\n('Y',TO_CHAR(SYSDATE, 'yyyyMMdd') || TO_CHAR((SELECT NVL(MAX(SEQ), 0) + 1 FROM EWS_BOARD WHERE TO_CHAR(CR_DATE, 'yyyyMMdd') = TO_CHAR(SYSDATE, 'yyyyMMdd')),'FM000')," +
				"(SELECT NVL(MAX(SEQ), 0) + 1 FROM EWS_BOARD WHERE TO_CHAR(CR_DATE, 'yyyyMMdd') = TO_CHAR(SYSDATE, 'yyyyMMdd'))," +
				"?,?,?,?,?,?)";
		
		sybaseQuery = null;
		oracleQuery = null;
		mssqlQuery = null;
		mysqlQuery = null;
		db2Query = null;
		informixQuery = null;

		dynamicQuery = new String[] {
			
		};

		paramData = new String[] {
			"TITLE", "CONTENT", "CR_EMPID", "CR_NAME", "FILE_ID", "DASH_USE"
		};
		paramDataType = new int[] { Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING  };
		paramMode = new int[] {Constants.IN, Constants.IN, Constants.IN, Constants.IN, Constants.IN, Constants.IN};

		returnMSG = "";
		returnType = Constants.ENTITY;
		returnData = new String[][] {
			
		};
		returnDataType = new int[] {  };
	}
}
