/*
 * Copyright (c) 2005 inswave, Inc. All Rights Reserved.
 *
 */
package com.ews.sta.xda;

import com.inswave.system.xda.*;
/**
 * com.ews.ope.xda.Opermng019_001
 */
public class StatInfo006_008 extends AccessInfo {
	public StatInfo006_008() {
		type = Constants.SELECT;
		defaultQuery = "SELECT a.SEQ\n" +
				", a.REF_ID\n " +
				", a.TITLE\n  " +
				", a.CONTENT \n  " +
				", TO_CHAR(a.CR_DATE, 'yyyyMMdd') AS CR_DATE  \n  " +
				", a.CR_EMPID\n  " +
				", b.EMP_NAME\n" +
				", a.CNT\n" +
				", a.DASH_USE\n" +
				"FROM EWS_BOARD a\n" +
				"LEFT OUTER JOIN EWS_EMP b\n" +
				"ON a.CR_EMPID = b.EMP_ID\n" +
				"WHERE  TO_CHAR(a.CR_DATE, 'yyyyMMdd') BETWEEN ? AND ?\n" +
				"AND b.EMP_NAME = ?\n" +
				"AND USE_YN = 'Y'\n" +
				"ORDER BY CR_DATE DESC, SEQ DESC\n";
		sybaseQuery = null;
		oracleQuery = null;
		mssqlQuery = null;
		mysqlQuery = null;
		db2Query = null;
		informixQuery = null;

		dynamicQuery = new String[] {
			
		};

		paramData = new String[] {
			"ST_DT","ED_DT","IN_CODE"
		};
		paramDataType = new int[] {Constants.STRING,Constants.STRING,Constants.STRING};//Constants.STRING, Constants.STRING
		paramMode = new int[] {Constants.IN,Constants.IN,Constants.IN};//Constants.IN, Constants.IN

		returnMSG = "RESULT";
		returnType = Constants.VECTOR;
		returnData = new String[][] {
			{"REF_ID", "REF_ID"},
			{"SEQ", "SEQ"},
			{"TITLE", "TITLE"},
			{"CONTENT", "CONTENT"},
			{"CR_DATE", "CR_DATE"},
			{"CR_EMPID", "CR_EMPID"},
			{"EMP_NAME", "EMP_NAME"},
			{"CNT", "CNT"},
			{"DASH_USE", "DASH_USE"}
		};
		returnDataType = new int[] { Constants.STRING,  Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING };
	}
}
