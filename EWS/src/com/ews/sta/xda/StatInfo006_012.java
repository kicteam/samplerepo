/*
 * Copyright (c) 2005 inswave, Inc. All Rights Reserved.
 *
 */
package com.ews.sta.xda;

import com.inswave.system.xda.*;
/**
 * com.ews.com.xda.Common009_012
 */
public class StatInfo006_012 extends AccessInfo {
	public StatInfo006_012() {
		type = Constants.UPDATE;
		defaultQuery = "UPDATE EWS_BOARD \n " +
				"SET USE_YN = 'N'\n" +
				"WHERE REF_ID = ?";
		sybaseQuery = null;
		oracleQuery = null;
		mssqlQuery = null;
		mysqlQuery = null;
		db2Query = null;
		informixQuery = null;

		dynamicQuery = new String[] {
			
		};

		paramData = new String[] {
			"REF_ID"
		};
		paramDataType = new int[] { Constants.STRING };
		paramMode = new int[] {Constants.IN };

		returnMSG = "";
		returnType = Constants.ENTITY;
		returnData = new String[][] {
			
		};
		returnDataType = new int[] {  };
	}
}
