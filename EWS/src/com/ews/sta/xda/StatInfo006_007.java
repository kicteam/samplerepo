/*
 * Copyright (c) 2005 inswave, Inc. All Rights Reserved.
 *
 */
package com.ews.sta.xda;

import com.inswave.system.xda.*;
/**
 * com.ews.com.xda.Opermng019_007"
 */
public class StatInfo006_007 extends AccessInfo {
	public StatInfo006_007() {
		type = Constants.DELETE;
		defaultQuery = "DELETE FROM EWS_BFILE WHERE FL_ID = ? AND FL_SEQ = ?";
		sybaseQuery = null;
		oracleQuery = null;
		mssqlQuery = null;
		mysqlQuery = null;
		db2Query = null;
		informixQuery = null;

		dynamicQuery = new String[] {
			
		};

		paramData = new String[] {
			"FL_ID", "FL_SEQ"
		};
		paramDataType = new int[] {Constants.STRING, Constants.STRING };
		paramMode = new int[] { Constants.IN, Constants.IN};

		returnMSG = "";
		returnType = Constants.ENTITY;
		returnData = new String[][] {
			
		};
		returnDataType = new int[] {  };
	}
}
