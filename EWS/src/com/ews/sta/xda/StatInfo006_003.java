/*
 * Copyright (c) 2005 inswave, Inc. All Rights Reserved.
 *
 */
package com.ews.sta.xda;

import com.inswave.system.xda.*;
/**
 * com.ews.ope.xda.Opermng019_001
 */
public class StatInfo006_003 extends AccessInfo {
	public StatInfo006_003() {
		type = Constants.INSERT;
		defaultQuery = "INSERT INTO EWS_BFILE\n  (FL_ID, FL_SEQ, FL_PATH, FL_RNAME, FL_SNAME, FL_SIZE, FL_TYPE)\nVALUES\n  (?,\n NVL((SELECT MAX(FL_SEQ) + 1 FROM EWS_BFILE WHERE FL_ID = ?), '1'),\n   ?,\n   ?,\n   ?||'_'||NVL((SELECT MAX(FL_SEQ) + 1 FROM EWS_BFILE WHERE FL_ID = ?), '1'),\n   ?,\n   ?)";
		sybaseQuery = null;
		oracleQuery = null;
		mssqlQuery = null;
		mysqlQuery = null;
		db2Query = null;
		informixQuery = null;

		dynamicQuery = new String[] {
			
		};

		paramData = new String[] {
				"FL_ID", "FL_ID", "FL_PATH", "FL_RNAME", "FL_ID", "FL_ID", "FL_SIZE", "FL_TYPE"
		};
		paramDataType = new int[] { Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING };
		paramMode = new int[] {Constants.IN, Constants.IN, Constants.IN, Constants.IN, Constants.IN, Constants.IN, Constants.IN, Constants.IN};

		returnMSG = "";
		returnType = Constants.ENTITY;
		returnData = new String[][] {
			
		};
		returnDataType = new int[] {  };
	}
}
