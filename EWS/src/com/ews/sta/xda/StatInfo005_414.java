/*
 * Copyright (c) 2005 inswave, Inc. All Rights Reserved.
 *
 */
package com.ews.sta.xda;

import com.inswave.system.xda.*;
/**
 * com.ews.sta.xda.StatInfo005_414
 */
public class StatInfo005_414 extends AccessInfo {
	public StatInfo005_414() {
		type = Constants.SELECT;
		defaultQuery = "SELECT \n        \"시나리오ID\"\n      , \"시나리오명\"\n      , \"시나리오등급\"\n      , \"모니터링주기\" \n      ," +
				" \"투자관리번호\"\n, \"투자관리명\"\n, \"WBS승인일\"\n, \"최종승인부서\"\n, \"최종승인자\"\n, " + 
				" \"PostionID\"\n, \"책임CostCenter\"\n, \"책임CostCenter명\"\n, \"투자담당부서\"\n, \"신청부서\"\n, " +  
				" \"투자승인금액\"\n, \"기간시작일\"\n, \"기간종료일\"\n, \"WBS실적금액\"\n, \"처리일\"\n, " + 
				" \"PR번호\"\n, \"생성부서\"\n, \"생성자\"\n, \"승인부서\"\n, \"PR최종승인자\"\n, " +
				" \"최종승인일\"\n, \"PR금액\"\n, \"경과일자\"\n, " +
				" \"EWS가동일\"\n      , \"실무상태\"   \n      , \"실무승인자\"\n      , \"실무승인자명\"   \n      , \"주관상태\"   \n      , \"주관승인자\"\n      , \"주관승인자명\"\n      ,\"[실무]점검문서번호\" AS  \"l실무l점검문서번호\"\n      ,\"[실무]점검제목\" AS \"l실무l점검제목\"\n      ,\"[실무]점검문서작성일\" AS \"l실무l점검문서작성일\" \n      ,\"[실무]점검부서\" AS \"l실무l점검부서\"\n      ,\"[실무]점검부서명\" AS \"l실무l점검부서명\"     \n      ,\"[실무]점검자\" AS \"l실무l점검자\"\n      ,\"[실무]점검자명\" AS \"l실무l점검자명\"      \n      ,\"[실무]발생원인\" AS \"l실무l발생원인\"    \n      ,\"[실무]처리방안\" AS \"l실무l처리방안\"     \n      ,\"[실무]진행상태\" AS \"l실무l진행상태\"\n      ,\"[실무]승인자\" AS \"l실무l승인자\"\n      ,\"[실무]승인자명\" AS \"l실무l승인자명\"    \n      ,\"[실무]승인일자\" AS \"l실무l승인일자\"   \n      ,\"[실무]승인자코멘트\" AS \"l실무l승인자코멘트\"\n      ,\"[주관]점검문서번호\" AS \"l주관l점검문서번호\"       \n      ,\"[주관]점검제목\" AS \"l주관l점검제목\"    \n      ,\"[주관]점검문서작성일\" AS \"l주관l점검문서작성일\"     \n      ,\"[주관]점검부서\" AS \"l주관l점검부서\"\n      ,\"[주관]점검부서명\" AS \"l주관l점검부서명\"     \n      ,\"[주관]점검자\" AS \"l주관l점검자\" \n      ,\"[주관]점검자명\" AS \"l주관l점검자명\"      \n      ,\"[주관]발생원인\" AS \"l주관l발생원인\"   \n      ,\"[주관]처리방안\" AS \"l주관l처리방안\"\n      ,\"[주관]진행상태\" AS \"l주관l진행상태\"  \n      ,\"[주관]승인자\" AS \"l주관l승인자\" \n      ,\"[주관]승인자명\" AS \"l주관l승인자명\"  \n      ,\"[주관]승인일자\" AS \"l주관l승인일자\"   \n      ,\"[주관]승인자코멘트\" AS \"l주관l승인자코멘트\"\nFROM EWS_SP414_V\nWHERE \"EWS가동일\" BETWEEN ? AND ?";
		sybaseQuery = null;
		oracleQuery = null;
		mssqlQuery = null;
		mysqlQuery = null;
		db2Query = null;
		informixQuery = null;

		dynamicQuery = new String[] {
			
		};

		paramData = new String[] {
			"ST_DT", "ED_DT"
		};
		paramDataType = new int[] { Constants.STRING, Constants.STRING };
		paramMode = new int[] {Constants.IN, Constants.IN};

		returnMSG = "RESULT";
		returnType = Constants.VECTOR;
		returnData = new String[][] {
			{"시나리오ID", "시나리오ID"},
			{"시나리오명", "시나리오명"},
			{"시나리오등급", "시나리오등급"},
			{"모니터링주기", "모니터링주기"},
			
			{"투자관리번호", "투자관리번호"},
			{"투자관리명", "투자관리명"},
			{"WBS승인일", "WBS승인일"},
			{"최종승인부서", "최종승인부서"},
			{"최종승인자", "최종승인자"},
			{"PostionID", "PostionID"},
			{"책임CostCenter", "책임CostCenter"},
			{"책임CostCenter명", "책임CostCenter명"},
			{"투자담당부서", "투자담당부서"},
			{"신청부서", "신청부서"},
			{"투자승인금액", "투자승인금액"}, 
			{"기간시작일", "기간시작일"},
			{"기간종료일", "기간종료일"},
			{"WBS실적금액", "WBS실적금액"},
			{"처리일", "처리일"},
			{"PR번호", "PR번호"},
			{"생성부서", "생성부서"},
			{"생성자", "생성자"},
			{"승인부서", "승인부서"},
			{"PR최종승인자", "PR최종승인자"},
			{"최종승인일", "최종승인일"},
			{"PR금액", "PR금액"},
			{"경과일자", "경과일자"},

			{"EWS가동일", "EWS가동일"},
			{"실무상태", "실무상태"},
			{"실무승인자", "실무승인자"},
			{"실무승인자명", "실무승인자명"},
			{"주관상태", "주관상태"},
			{"주관승인자", "주관승인자"},
			{"주관승인자명", "주관승인자명"},
			{"l실무l점검문서번호", "l실무l점검문서번호"},
			{"l실무l점검제목", "l실무l점검제목"},
			{"l실무l점검문서작성일", "l실무l점검문서작성일"},
			{"l실무l점검부서", "l실무l점검부서"},
			{"l실무l점검부서명", "l실무l점검부서명"},
			{"l실무l점검자", "l실무l점검자"},
			{"l실무l점검자명", "l실무l점검자명"},
			{"l실무l발생원인", "l실무l발생원인"},
			{"l실무l처리방안", "l실무l처리방안"},
			{"l실무l진행상태", "l실무l진행상태"},
			{"l실무l승인자", "l실무l승인자"},
			{"l실무l승인자명", "l실무l승인자명"},
			{"l실무l승인일자", "l실무l승인일자"},
			{"l실무l승인자코멘트", "l실무l승인자코멘트"},
			{"l주관l점검문서번호", "l주관l점검문서번호"},
			{"l주관l점검제목", "l주관l점검제목"},
			{"l주관l점검문서작성일", "l주관l점검문서작성일"},
			{"l주관l점검부서", "l주관l점검부서"},
			{"l주관l점검부서명", "l주관l점검부서명"},
			{"l주관l점검자", "l주관l점검자"},
			{"l주관l점검자명", "l주관l점검자명"},
			{"l주관l발생원인", "l주관l발생원인"},
			{"l주관l처리방안", "l주관l처리방안"},
			{"l주관l진행상태", "l주관l진행상태"},
			{"l주관l승인자", "l주관l승인자"},
			{"l주관l승인자명", "l주관l승인자명"},
			{"l주관l승인일자", "l주관l승인일자"},
			{"l주관l승인자코멘트", "l주관l승인자코멘트"}
		};
		returnDataType = new int[] { Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING};
	}
}
