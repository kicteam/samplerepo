/*
 * Copyright (c) 2005 inswave, Inc. All Rights Reserved.
 *
 */
package com.ews.sta.xda;

import com.inswave.system.xda.*;
/**
 * com.ews.ope.xda.Opermng019_004
 */
public class StatInfo006_004 extends AccessInfo {
	public StatInfo006_004() {
		type = Constants.SELECT;
		defaultQuery = "SELECT (?||'_'||NVL((SELECT MAX(FL_SEQ) + 1 FROM EWS_BFILE WHERE FL_ID = ?), '1')) AS FL_SNAME FROM DUAL";
		sybaseQuery = null;
		oracleQuery = null;
		mssqlQuery = null;
		mysqlQuery = null;
		db2Query = null;
		informixQuery = null;

		dynamicQuery = new String[] {
			
		};

		paramData = new String[] {
			"FL_ID", "FL_ID"
		};
		paramDataType = new int[] { Constants.STRING, Constants.STRING };
		paramMode = new int[] {Constants.IN, Constants.IN};

		returnMSG = "RESULT";
		returnType = Constants.ENTITY;
		returnData = new String[][] {
			{"FL_SNAME", "FL_SNAME"}
		};
		returnDataType = new int[] { Constants.STRING };
	}
}
