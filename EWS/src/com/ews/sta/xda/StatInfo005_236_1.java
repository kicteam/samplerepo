/*
 * Copyright (c) 2005 inswave, Inc. All Rights Reserved.
 *
 */
package com.ews.sta.xda;

import com.inswave.system.xda.*;
/**
 * com.ews.sta.xda.StatInfo005_236_1
 */
public class StatInfo005_236_1 extends AccessInfo {
	public StatInfo005_236_1() {
		type = Constants.SELECT;
		defaultQuery = "SELECT \n        \"시나리오ID\"\n      , \"시나리오명\"\n      , \"시나리오등급\"\n      , \"모니터링주기\" \n      , \"고객그룹\"        \n      , \"고객그룹명\"     \n      , \"고객코드\"        \n      , \"고객명\"     \n      , \"자재코드\"        \n      , \"자재명\"        \n      , \"가격적용일\"        \n      , \"가격마스터단가\"  \n      , \"기본할인액\"\n      , \"빌링단가\"  \n      , \"추가할인액\"       \n      , \"추가할인율\"          \n      , \"문서번호\"        \n      , \"대금청구수량\"        \n      , \"대금청구단위\"        \n      , \"빌링금액\"        \n      , \"통화단위\"        \n      , \"기준가구분\"        \n      , \"판매조직\"        \n      , \"유통경로\"        \n      , \"제품군\"        \n      , \"사업장\"        \n      , \"영업그룹\"        \n      , \"영업그룹명\"     \n      , \"생성일\"        \n      , \"생성자\"        \n      , \"생성자명\"   \n      , \"효력시작일\"        \n      , \"효력종료일\"        \n      , \"EWS가동일\"\n      , \"실무상태\"   \n      , \"실무승인자\"\n      , \"실무승인자명\"   \n      , \"주관상태\"   \n      , \"주관승인자\"\n      , \"주관승인자명\"\n      ,\"[실무]점검문서번호\" AS  \"l실무l점검문서번호\"\n      ,\"[실무]점검제목\" AS \"l실무l점검제목\"\n      ,\"[실무]점검문서작성일\" AS \"l실무l점검문서작성일\" \n      ,\"[실무]점검부서\" AS \"l실무l점검부서\"\n      ,\"[실무]점검부서명\" AS \"l실무l점검부서명\"     \n      ,\"[실무]점검자\" AS \"l실무l점검자\"\n      ,\"[실무]점검자명\" AS \"l실무l점검자명\"      \n      ,\"[실무]발생원인\" AS \"l실무l발생원인\"    \n      ,\"[실무]처리방안\" AS \"l실무l처리방안\"     \n      ,\"[실무]진행상태\" AS \"l실무l진행상태\"\n      ,\"[실무]승인자\" AS \"l실무l승인자\"\n      ,\"[실무]승인자명\" AS \"l실무l승인자명\"    \n      ,\"[실무]승인일자\" AS \"l실무l승인일자\"   \n      ,\"[실무]승인자코멘트\" AS \"l실무l승인자코멘트\"\n      ,\"[주관]점검문서번호\" AS \"l주관l점검문서번호\"       \n      ,\"[주관]점검제목\" AS \"l주관l점검제목\"    \n      ,\"[주관]점검문서작성일\" AS \"l주관l점검문서작성일\"     \n      ,\"[주관]점검부서\" AS \"l주관l점검부서\"\n      ,\"[주관]점검부서명\" AS \"l주관l점검부서명\"     \n      ,\"[주관]점검자\" AS \"l주관l점검자\" \n      ,\"[주관]점검자명\" AS \"l주관l점검자명\"      \n      ,\"[주관]발생원인\" AS \"l주관l발생원인\"   \n      ,\"[주관]처리방안\" AS \"l주관l처리방안\"\n      ,\"[주관]진행상태\" AS \"l주관l진행상태\"  \n      ,\"[주관]승인자\" AS \"l주관l승인자\" \n      ,\"[주관]승인자명\" AS \"l주관l승인자명\"  \n      ,\"[주관]승인일자\" AS \"l주관l승인일자\"   \n      ,\"[주관]승인자코멘트\" AS \"l주관l승인자코멘트\"   \nFROM EWS_SP236_V\nWHERE \"생성일\" BETWEEN ? AND ?";
		sybaseQuery = null;
		oracleQuery = null;
		mssqlQuery = null;
		mysqlQuery = null;
		db2Query = null;
		informixQuery = null;

		dynamicQuery = new String[] {
			
		};

		paramData = new String[] {
			"ST_DT", "ED_DT"
		};
		paramDataType = new int[] { Constants.STRING, Constants.STRING };
		paramMode = new int[] {Constants.IN, Constants.IN};

		returnMSG = "RESULT";
		returnType = Constants.VECTOR;
		returnData = new String[][] {
			{"시나리오ID", "시나리오ID"},
			{"시나리오명", "시나리오명"},
			{"시나리오등급", "시나리오등급"},
			{"모니터링주기", "모니터링주기"},
			{"고객그룹", "고객그룹"},
			{"고객그룹명", "고객그룹명"},
			{"고객코드", "고객코드"},
			{"고객명", "고객명"},
			{"자재코드", "자재코드"},
			{"자재명", "자재명"},
			{"가격적용일", "가격적용일"},
			{"가격마스터단가", "가격마스터단가"},
			{"기본할인액", "기본할인액"},
			{"빌링단가", "빌링단가"},
			{"추가할인액", "추가할인액"},
			{"추가할인율", "추가할인율"},
			{"문서번호", "문서번호"},
			{"대금청구수량", "대금청구수량"},
			{"대금청구단위", "대금청구단위"},
			{"빌링금액", "빌링금액"},
			{"통화단위", "통화단위"},
			{"기준가구분", "기준가구분"},
			{"판매조직", "판매조직"},
			{"유통경로", "유통경로"},
			{"제품군", "제품군"},
			{"사업장", "사업장"},
			{"영업그룹", "영업그룹"},
			{"영업그룹명", "영업그룹명"},
			{"생성일", "생성일"},
			{"생성자", "생성자"},
			{"생성자명", "생성자명"},
			{"효력시작일", "효력시작일"},
			{"효력종료일", "효력종료일"},
			{"EWS가동일", "EWS가동일"},
			{"실무상태", "실무상태"},
			{"실무승인자", "실무승인자"},
			{"실무승인자명", "실무승인자명"},
			{"주관상태", "주관상태"},
			{"주관승인자", "주관승인자"},
			{"주관승인자명", "주관승인자명"},
			{"l실무l점검문서번호", "l실무l점검문서번호"},
			{"l실무l점검제목", "l실무l점검제목"},
			{"l실무l점검문서작성일", "l실무l점검문서작성일"},
			{"l실무l점검부서", "l실무l점검부서"},
			{"l실무l점검부서명", "l실무l점검부서명"},
			{"l실무l점검자", "l실무l점검자"},
			{"l실무l점검자명", "l실무l점검자명"},
			{"l실무l발생원인", "l실무l발생원인"},
			{"l실무l처리방안", "l실무l처리방안"},
			{"l실무l진행상태", "l실무l진행상태"},
			{"l실무l승인자", "l실무l승인자"},
			{"l실무l승인자명", "l실무l승인자명"},
			{"l실무l승인일자", "l실무l승인일자"},
			{"l실무l승인자코멘트", "l실무l승인자코멘트"},
			{"l주관l점검문서번호", "l주관l점검문서번호"},
			{"l주관l점검제목", "l주관l점검제목"},
			{"l주관l점검문서작성일", "l주관l점검문서작성일"},
			{"l주관l점검부서", "l주관l점검부서"},
			{"l주관l점검부서명", "l주관l점검부서명"},
			{"l주관l점검자", "l주관l점검자"},
			{"l주관l점검자명", "l주관l점검자명"},
			{"l주관l발생원인", "l주관l발생원인"},
			{"l주관l처리방안", "l주관l처리방안"},
			{"l주관l진행상태", "l주관l진행상태"},
			{"l주관l승인자", "l주관l승인자"},
			{"l주관l승인자명", "l주관l승인자명"},
			{"l주관l승인일자", "l주관l승인일자"},
			{"l주관l승인자코멘트", "l주관l승인자코멘트"}
		};
		returnDataType = new int[] { Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING };
	}
}
