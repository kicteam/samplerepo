/*
 * Copyright (c) 2005 inswave, Inc. All Rights Reserved.
 *
 */
package com.ews.sta.xda;

import com.inswave.system.xda.*;
/**
 * com.ews.ope.xda.Opermng019_006
 */
public class StatInfo006_006 extends AccessInfo {
	public StatInfo006_006() {
		type = Constants.SELECT;
		defaultQuery = "SELECT FL_ID,\n" +
				"FL_SEQ,\n" +
				"FL_PATH,\n" +
				"FL_RNAME,\n" +
				"FL_SNAME,\n" +
				"FL_SIZE,\n" +
				"FL_TYPE\n" +
				"FROM EWS_BFILE\n" +
				"WHERE FL_ID = ?";
		sybaseQuery = null;
		oracleQuery = null;
		mssqlQuery = null;
		mysqlQuery = null;
		db2Query = null;
		informixQuery = null;

		dynamicQuery = new String[] {
			
		};

		paramData = new String[] {
			"FILE_ID"
		};
		paramDataType = new int[] {Constants.STRING };//Constants.STRING, Constants.STRING };
		paramMode = new int[] {Constants.IN };//Constants.IN, Constants.IN};

		returnMSG = "RESULT";
		returnType = Constants.VECTOR;
		returnData = new String[][] {
				{"FL_ID", "FL_ID"},
				{"FL_SEQ", "FL_SEQ"},
				{"FL_PATH", "FL_PATH"},
				{"FL_RNAME", "FL_RNAME"},
				{"FL_SNAME", "FL_SNAME"},
				{"FL_SIZE", "FL_SIZE"},
				{"FL_TYPE", "FL_TYPE"}
		};
		returnDataType = new int[] { Constants.STRING, Constants.STRING,  Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING };
	}
}
