/*
 * Copyright (c) 2005 inswave, Inc. All Rights Reserved.
 *
 */
package com.ews.sta.xda;

import com.inswave.system.xda.*;
/**
 * com.ews.com.xda.Common009_011
 */
public class StatInfo006_011 extends AccessInfo {
	public StatInfo006_011() {
		type = Constants.UPDATE;
		defaultQuery = "UPDATE EWS_BOARD \n " +
				"SET CONTENT = ?,\n" +
				"FL_ID = ?\n" +
				"DASH_USE = ?\n" +
				"WHERE REF_ID = ?";
		sybaseQuery = null;
		oracleQuery = null;
		mssqlQuery = null;
		mysqlQuery = null;
		db2Query = null;
		informixQuery = null;

		dynamicQuery = new String[] {
			
		};

		paramData = new String[] {
			"CONTENT",  "FILE_ID", "DASH_USE","REF_ID"
		};
		paramDataType = new int[] { Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING };
		paramMode = new int[] {Constants.IN, Constants.IN, Constants.IN, Constants.IN };

		returnMSG = "";
		returnType = Constants.ENTITY;
		returnData = new String[][] {
			
		};
		returnDataType = new int[] {  };
	}
}
