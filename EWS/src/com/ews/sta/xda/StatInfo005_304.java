/*
 * Copyright (c) 2005 inswave, Inc. All Rights Reserved.
 *
 */
package com.ews.sta.xda;

import com.inswave.system.xda.*;
/**
 * com.ews.sta.xda.StatInfo005_304
 */
public class StatInfo005_304 extends AccessInfo {
	public StatInfo005_304() {
		type = Constants.SELECT;
		defaultQuery = "SELECT \n" +
				"        \"시나리오ID\"\n" +
				"      , \"시나리오명\"\n" +
				"      , \"시나리오등급\"\n" +
				"      , \"모니터링주기\" \n" +
				"      , \"판매문서번호\"        \n" +
				"      , \"품목번호\"        \n" +
				"      , \"자재코드\"        \n" +
				"      , \"자재명\"        \n" +
				"      , \"통화\"        \n" +
				"      , \"수량\"       \n" +
				"      , \"단가\"        \n" +
				"      , \"단위\"        \n" +
				"      , \"생성자ID\"        \n" +
				"      , \"생성자명\"   \n" +
				"      , \"주문입력일\"        \n" +
				"      , \"가격결정일\"        \n" +
				"      , \"차이일수\"  \n" +
				"      , \"출하일(빌링일)\" AS \"출하일I빌링일I\"       \n" +
				"      , \"고객코드\"        \n" +
				"      , \"고객명\"     \n" +
				"      , \"판매조직\"        \n" +
				"      , \"유통경로\"        \n" +
				"      , \"제품군\"        \n" +
				"      , \"사업장\"        \n" +
				"      , \"영업그룹\"        \n" +
				"      , \"영업그룹명\"     \n" +
				"      , \"EWS가동일\"\n" +
				"      , \"실무상태\"   \n" +
				"      , \"실무승인자\"\n" +
				"      , \"실무승인자명\"   \n" +
				"      , \"주관상태\"   \n" +
				"      , \"주관승인자\"\n" +
				"      , \"주관승인자명\"\n" +
				"      , \"[실무]점검문서번호\"       \n" +
				"      , \"[실무]점검제목\"    \n" +
				"      , \"[실무]점검문서작성일\"     \n" +
				"      , \"[실무]점검부서\"\n" +
				"      , \"[실무]점검부서명\"     \n" +
				"      , \"[실무]점검자\"\n" +
				"      , \"[실무]점검자명\"      \n" +
				"      , \"[실무]발생원인\"    \n" +
				"      , \"[실무]처리방안\"     \n" +
				"      , \"[실무]진행상태\"   \n" +
				"      , \"[실무]승인자\"\n" +
				"      , \"[실무]승인자명\"     \n" +
				"      , \"[실무]승인일자\"   \n" +
				"      , \"[실무]승인자코멘트\"\n" +
				"      , \"[주관]점검문서번호\"       \n" +
				"      , \"[주관]점검제목\"    \n" +
				"      , \"[주관]점검문서작성일\"     \n" +
				"      , \"[주관]점검부서\"\n" +
				"      , \"[주관]점검부서명\"     \n" +
				"      , \"[주관]점검자\"\n" +
				"      , \"[주관]점검자명\"      \n" +
				"      , \"[주관]발생원인\"    \n" +
				"      , \"[주관]처리방안\"     \n" +
				"      , \"[주관]진행상태\"   \n" +
				"      , \"[주관]승인자\"   \n" +
				"      , \"[주관]승인자명\"  \n" +
				"      , \"[주관]승인일자\"   \n" +
				"      , \"[주관]승인자코멘트\"   \n" +
				"FROM EWS_SP304_V\n" +
				"WHERE \"EWS가동일\" BETWEEN ? AND ?";
		
		sybaseQuery = null;
		oracleQuery = null;
		mssqlQuery = null;
		mysqlQuery = null;
		db2Query = null;
		informixQuery = null;

		dynamicQuery = new String[] {
			
		};

		paramData = new String[] {
			"ST_DT", "ED_DT"
		};
		paramDataType = new int[] { Constants.STRING, Constants.STRING };
		paramMode = new int[] {Constants.IN, Constants.IN};

		returnMSG = "RESULT";
		returnType = Constants.VECTOR;
		returnData = new String[][] {
			{"시나리오ID", "시나리오ID"},
			{"시나리오명", "시나리오명"},
			{"시나리오등급", "시나리오등급"},
			{"모니터링주기", "모니터링주기"},
			{"판매문서번호", "판매문서번호"},
			{"품목번호", "품목번호"},
			{"자재코드", "자재코드"},
			{"자재명", "자재명"},
			{"통화", "통화"},
			{"수량", "수량"},
			{"단가", "단가"},
			{"단위", "단위"},
			{"생성자ID", "생성자ID"},
			{"생성자명", "생성자명"},
			{"주문입력일", "주문입력일"},
			{"가격결정일", "가격결정일"},    
			{"차이일수", "차이일수"},
			{"출하일I빌링일I", "출하일I빌링일I"},
			{"고객코드", "고객코드"},
			{"고객명", "고객명"},
			{"판매조직", "판매조직"},
			{"유통경로", "유통경로"},
			{"제품군", "제품군"},
			{"사업장", "사업장"},
			{"영업그룹", "영업그룹"},
			{"영업그룹명", "영업그룹명"},
			{"EWS가동일", "EWS가동일"},
			{"실무상태", "실무상태"},
			{"실무승인자", "실무승인자"},
			{"실무승인자명", "실무승인자명"},
			{"주관상태", "주관상태"},
			{"주관승인자", "주관승인자"},
			{"주관승인자명", "주관승인자명"},
			{"[실무]점검문서번호", "[실무]점검문서번호"},
			{"[실무]점검제목", "[실무]점검제목"},
			{"[실무]점검문서작성일", "[실무]점검문서작성일"},
			{"[실무]점검부서", "[실무]점검부서"},
			{"[실무]점검부서명", "[실무]점검부서명"},
			{"[실무]점검자", "[실무]점검자"},
			{"[실무]점검자명", "[실무]점검자명"},
			{"[실무]발생원인", "[실무]발생원인"},
			{"[실무]처리방안", "[실무]처리방안"},
			{"[실무]진행상태", "[실무]진행상태"},
			{"[실무]승인자", "[실무]승인자"},
			{"[실무]승인자명", "[실무]승인자명"},
			{"[실무]승인일자", "[실무]승인일자"},
			{"[실무]승인자코멘트", "[실무]승인자코멘트"},
			{"[주관]점검문서번호", "[주관]점검문서번호"},
			{"[주관]점검제목", "[주관]점검제목"},
			{"[주관]점검문서작성일", "[주관]점검문서작성일"},
			{"[주관]점검부서", "[주관]점검부서"},
			{"[주관]점검부서명", "[주관]점검부서명"},
			{"[주관]점검자", "[주관]점검자"},
			{"[주관]점검자명", "[주관]점검자명"},
			{"[주관]발생원인", "[주관]발생원인"},
			{"[주관]처리방안", "[주관]처리방안"},
			{"[주관]진행상태", "[주관]진행상태"},
			{"[주관]승인자", "[주관]승인자"},
			{"[주관]승인자명", "[주관]승인자명"},
			{"[주관]승인일자", "[주관]승인일자"},
			{"[주관]승인자코멘트", "[주관]승인자코멘트"}
		};
		returnDataType = new int[] { Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING, Constants.STRING };
	}
}
