package com.ews.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.sql.SQLException;

import org.w3c.dom.Document;

import com.inswave.system.Task;
import com.inswave.system.exception.FrameworkWarning;
import com.inswave.system.exception.Warning;
import com.inswave.system.xda.Constants;
import com.inswave.system.xda.XDA;
import com.inswave.system.xda.XDAFactory;
import com.inswave.util.WarningUtil;
import com.inswave.util.XMLUtil;

public class BizCommon extends Task {

	private XDA commonXDA = null;
	
	/**
	 * WARNING 메시지레벨을 나타낸다. 
	 */
	public static final int MSG = 0;
	public static final int WARNING = 1;
	public static final int ERROR = 2;
	public static final int NONE = 3;
	
	/**
	 * getXDA (xda connection)
	 **/
	public XDA getXDA() throws Exception {
		this.commonXDA = XDAFactory.getXDA(EwsConstant.dbName, Constants.KEEP_CONNECTION);
		this.commonXDA.setAutoCommit(false);
		return this.commonXDA;
	}

	/**
	 * execute (조회 사용)
	 * @param	pXDAID		String		XDA 아이디
	 * @param	doc			Document	XML데이타
	 * @return	result		Document	처리결과
	 * @exception	e		Exception	예외처리
	 **/
	public Document execute(String pXDAID, Document doc) throws Exception {

		Document result = null;
		XDA xda = null;
		
		try {
			//XMLUtil.setDebug(doc, true);
			xda = this.getXDA();

			result = xda.execute(pXDAID, doc);

		}catch(FrameworkWarning fw){
            throw fw;
        }catch(Warning warning){
            throw warning;
        }catch(Exception e){
        	e.printStackTrace();
        	Warning warning = this.makeWarning("ERROR", Warning.ERROR, "execute error", "", result, 0, "", e);
            throw warning;
        }

		return result;
	}
	
	/**
	 * makeWarning (warning 메세지 만들기)
	 * @param	
	 * @param	
	 * @return	warning	warning	만들어진 warning 메세지
	 **/
	public Warning makeWarning(String msg, int level, String detail, String errorCode, Document doc, int docType, String screenID, Throwable t) throws Exception {
		Document result = WarningUtil.makeWarningDoc(msg, level, detail, errorCode);
		XMLUtil.setString(result, "screenID", screenID);
		Warning warning = new Warning(result, t);
		warning.setAdditionalDocument(doc, docType);

		Throwable t2 = t.getCause();
		if ( t2 == null ) {
			t2 = t;
		}
		if (t2 instanceof SQLException) {
			SQLException se = (SQLException)t2;
			warning.setAdditionalCode(se.getErrorCode());
		}
		return warning;
	}
	
	/**
	 * 파일간 복사를 구현. XML 파일만이 아니라 다른 파일에도 적용가능하다.
	 * @param sourceLocation 복사 대상 파일 위치
	 * @param fileLocation 저장될 파일 위치
	 * @return void 값을 반환하지 않음 
	 * @throws Exception IOException 대상 파일을 만들지 못하였을때에 발생
	 */
	public boolean copyFile(String sourceLocation, String fileLocation) throws Exception{
		/* TODO 
		 * XML 데이터를 직접 바라보는 Repository 를 설정하여 위치를 GlobalContext에 설정하여 바라볼 수 있도록 한다.
		 * 이 데이터는 문서번호와 같은 이름을 하고 있다.
		 */
		
		//복사 대상이 되는 파일 생성
		File sourceFile = new File( sourceLocation );

		//스트림, 채널 선언
		FileInputStream inputStream = null;
		FileOutputStream outputStream = null;
		FileChannel fcin = null;
		FileChannel fcout = null;
		boolean flg = false;
		
		try {
			
		//스트림 생성
			inputStream = new FileInputStream(sourceFile);
			outputStream = new FileOutputStream(fileLocation);
			
		//채널 생성
			fcin = inputStream.getChannel();
			fcout = outputStream.getChannel();
		
		//채널을 통한 스트림 전송
			long size = fcin.size();
			fcin.transferTo(0, size, fcout);
			flg = true;
		} catch (Exception e) {
			throw e;
		} finally {
			
		//자원 해제
			if(fcout!=null) try{fcout.close();}catch(IOException ioe){}
			if(fcin!=null) try{fcin.close();}catch(IOException ioe){}
			if(outputStream!=null) try{outputStream.close();}catch(IOException ioe){}
			if(inputStream!=null) try{inputStream.close();}catch(IOException ioe){}
		}
		return flg;
	}
	
	/**
	 * 존재하는 파일을 삭제한다.
	 * @param filename
	 * @return 성공여부를 리턴한다. 성공 true (실패 false)
	 */
	public boolean removeFile(String filePath) throws Exception {
		boolean flg = false;
		try{
			File f = new File(filePath);
			flg = f.delete();
		}catch(Exception e){
			throw e;
		}
		return flg;
	}
}