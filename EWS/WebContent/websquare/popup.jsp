<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="" 
import="com.inswave.admin.*,com.inswave.system.*,com.inswave.system.exception.*,com.inswave.util.*,com.inswave.msg.warn.*,org.w3c.dom.*,com.inswave.system.context.*,com.ews.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns='http://www.w3.org/1999/xhtml' xmlns:ev='http://www.w3.org/2001/xml-events' xmlns:w2='http://www.inswave.com/websquare' xmlns:xf='http://www.w3.org/2002/xforms'>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<META http-equiv="X-UA-Compatible" content="IE=Edge"/>
		<title>	WebSquare </title>
		<script type="text/javascript" src="javascript.wq?q=/bootloader"></script>
		<script type="text/javascript" src="/EWS/Common/js/util.js"></script>
<%
	request.setCharacterEncoding("utf-8");
	String w2xPath = request.getParameter("w2xPath");
	String menu_id = request.getParameter("MENU_ID");
	String kri_id = request.getParameter("KRI_ID");
	
	String url = "";
	String params = "";
	
	
	if(w2xPath != null)  Logger.info("===> W2XPATH : " + w2xPath);
	if(menu_id != null)  Logger.info("===> MENU_ID : " + menu_id);
	if(kri_id != null)  Logger.info("===> KRI_ID : " + kri_id);
	
	FrameworkContext fc = FrameworkContext.getContext();
	fc.setWebInfo(request, response, null, null);
	
	if(w2xPath == null || "".equals(w2xPath)){
		
		//[menu_id]를 통한 url 조회
		if(menu_id == null || "".equals(menu_id)){
			;
		}else{
			url = getPageUrl(menu_id);
		}
		
		// [kri_id]를 통한 시나리오 url 조회
		if(kri_id == null || "".equals(kri_id)){
			;
		}else{
			url = getKriUrl(kri_id);
		}
		
		if(url != null)  Logger.info("===> URL Check !!! ["+ url+"]");
		
		if(url == null || "".equals(url)){
%>
			<script>
				ewsMsg.gfnAlert("g", "noPage");

				// 2014.07.14 운영은 위거를 사용 중
				top.document.location.href = contextPath+"?MENU_ID=00001";
				//top.document.location.href = "http://sso.s-oil.com/idms/sim/selfcare/login2.jsp";
			</script>
<%
		}else{  
			// param 만들기
			java.util.Enumeration paramEnum = request.getParameterNames();
			while(paramEnum.hasMoreElements()){
		 		String name=(String)paramEnum.nextElement();
		 		String value=request.getParameter(name);
		 			   value = java.net.URLEncoder.encode(value, "utf-8");
		 		params += "&";
		 		params += name + "=" + value;
			}
			
			Logger.info("===> URL INFO === /EWS/websquare/websquare.jsp?w2xPath=/EWS" + url + params);
			
%>
			<script>
				document.location.href = contextPath+"?w2xPath=/EWS<%=url + params%>";
			</script>	
<%
		}
	}else {
		//session
		EwsLoginInfoBean userInfo = (EwsLoginInfoBean) EwsLoginUtil.getSessionInfo();
		if(userInfo==null){
			if(!"00001".equals(menu_id)){
%>
			<script>
				// session 값이 없을시 로그인 페이지로
				ewsMsg.gfnAlert("g", "noSession");
				
				// 2014.07.14 운영은 위거를 사용 중
				top.document.location.href = contextPath+"?MENU_ID=00001";
				//top.document.location.href = "http://sso.s-oil.com/idms/sim/selfcare/login2.jsp";
			</script>
<%
			}
		}else{
			
			Logger.info("===> SESSION DOC == "+ XMLUtil.indent(userInfo.readUserDoc()));
			
			if("00001".equals(menu_id)){
%>
				<script>
				// session 값이 있을시 main 페이지로
					document.location.href = contextPath+"?MENU_ID=00000";
				</script>
<%				
			}
			
			java.lang.reflect.Method[] methods = userInfo.getClass().getMethods();
			for(int i = 0 ; i < methods.length ; i++) {
				String methodName = methods[i].getName();
				if(methodName.startsWith("get")) {
%>
					<script type="text/javascript" language="javascript">
					WebSquare.session.setAttribute("<%=methodName.substring(3)%>","<%=methods[i].invoke(userInfo, new Object[0])%>");
					</script>
<%					
				} else if(methodName.startsWith("is")) {	
%>
					<script type="text/javascript" language="javascript">
					WebSquare.session.setAttribute("<%=methodName.substring(2)%>","<%=methods[i].invoke(userInfo, new Object[0])%>");
					</script>
<%				
				}
			}
%>
					<script type="text/javascript" language="javascript">
					WebSquare.session.setAttribute("KRI_NM","<%=request.getParameter("MENU_NM") %>");
					</script>
					menu_id : <%=menu_id%>
					kri_id : <%=kri_id%>
			
			<script>
			// ssUserId 값이 null 일 경우
			if( WebSquare.session.getAttribute( "EMP_ID").isEmpty() || WebSquare.session.getAttribute( "EMP_ID")==null){
				ewsMsg.gfnAlert("g", "noSession");
				
				// 2014.07.14 운영은 위거를 사용 중
				top.document.location.href = contextPath+"?MENU_ID=00001";
				//top.document.location.href = "http://sso.s-oil.com/idms/sim/selfcare/login2.jsp";
				
			}
			</script>
<%				
		}
%>
		<script type="text/javascript">
			window.onload = init;

			function init() {
				try{
					WebSquare.startPopupApplication();
				} catch(e) {
					alert(e.message);
				}
			}
		</script>
<%	
	}
	fc.setWebInfo(null, null, null, null);	     
%>
	</head>
<body></body>
</html>

<%!     
// 메뉴 URL 조회
public String getPageUrl(String menu_id){
	String page_url = "";
	try {
		Document doc = null;
		doc = XMLUtil.getDocument("<request/>");
		XMLUtil.setTask( doc, "com.ews.com.service.CommonService" );
		XMLUtil.setAction( doc, "retrieve" );
		XMLUtil.setAttribute(doc, "PID", "5");
		XMLUtil.setString( doc, "MENU_ID", menu_id );
		Document retdoc = TaskUtil.perform( doc );
		if( retdoc != null ) {
			page_url = XMLUtil.getString(retdoc, "PAGE_URL");
		}
	} catch (Exception e) {
		Logger.exception("websquare.jsp", "getPageUrl", "Exception.", e);
	}
	return page_url;
}

// 시나리오 URL 조회
public String getKriUrl(String kri_id){
	String kri_url = "";
	try {
		Document doc = null;
		doc = XMLUtil.getDocument("<request/>");
		XMLUtil.setTask( doc, "com.ews.com.service.CommonService" );
		XMLUtil.setAction( doc, "retrieve" );
		XMLUtil.setAttribute(doc, "PID", "7");
		XMLUtil.setString( doc, "KRI_ID", kri_id );
		Document retdoc = TaskUtil.perform( doc );
		if( retdoc != null ) {
			kri_url = XMLUtil.getString(retdoc, "KRI_URL");
		}
	} catch (Exception e) {
		Logger.exception("websquare.jsp", "getKriUrl", "Exception.", e);
	}
	return kri_url;
}
%>