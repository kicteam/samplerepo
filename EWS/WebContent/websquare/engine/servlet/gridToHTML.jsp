<%@ page contentType="charset=UTF-8"
language="java"
errorPage=""
import="
java.util.*,
java.io.*,
org.w3c.dom.*,
com.inswave.msg.warn.*,
com.inswave.system.exception.*,
com.inswave.util.*"
%><%!
%><%

response.setDateHeader("Expires",0);
if( request.getProtocol().equals( "HTTP/1.1" ) ) {
	response.setHeader( "Cache-Control", "no-cache" );
} else {
	response.setHeader( "Prama", "no-cache" );
}
response.setContentType("application/octet-stream");


request.setCharacterEncoding( "UTF-8" );


ServletOutputStream				sos	= null;
FileOutputStream				fos = null;
java.io.BufferedOutputStream	bos = null;

try {
	String args = request.getParameter( "xmlValue" );

	if( args == null || args.equals("") ) {
		response.setContentType("text/xml");
		WARNINGMsg w = new WARNINGMsg();
		w.msg = "인자가 부족합니다.";
		w.level = Warning.WARNING;
		w.detail = "인자가 부족합니다.";
		out.println( w.toString() );
		return;
	}
	
	System.out.println("*******");
	System.out.println(args);
	System.out.println("********");
	Document doc = XMLUtil.getDocument( args );
	String	sFileName = XMLUtil.getAttribute(doc,"fileName");
	if(sFileName.equals("") || sFileName == null) {
		sFileName = "default.html";
	}
	response.setHeader( "Content-Disposition", "attachment;filename=" + sFileName + ";" );
	
	Hashtable hash = XMLUtil.toHashtable(doc);
	String htmlStr = (String)hash.get("data");
	byte[] htmlData = htmlStr.getBytes("euc-kr");
	
	sos = response.getOutputStream();
	bos = new java.io.BufferedOutputStream( sos );

	bos.write( htmlData );
	bos.flush();

	return;

} catch (Throwable e) {
//	Logger.exception( "xmlToExcel.jsp", "", "Exception.", e );
//	WARNINGMsg w = new WARNINGMsg();
//	w.msg = e.getMessage();
//	if( w.msg == null || w.msg.trim().equals("") ) {
//		w.msg = "ExceptionL ¡ˢ彿???";
//	}
//	w.level = Warning.ERROR;
//	w.detail = SystemUtil.getStackTrace( e );
//	System.out.println( w.toString() );
//	out.println( w.toString() );
	e.printStackTrace();
} finally {
	if( sos != null ) {
		try {
			sos.flush();
			sos.close();
			sos = null;
		} catch( Exception e ) {}
	}
	if( fos != null ) {
		try {
			fos.flush();
			fos.close();
			fos = null;
		} catch( Exception e ) {}
	}
	if( bos != null ) {
		try {
			bos.flush();
			bos.close();
			bos = null;
		} catch( Exception e ) {}
	}
}
%>