<%@ page contentType="text/xml; charset=UTF-8" language="java" errorPage="" import="com.inswave.admin.*,com.inswave.system.*,com.inswave.system.exception.*,com.inswave.util.*,com.inswave.msg.warn.*,org.w3c.dom.*,com.inswave.system.context.FrameworkContext,com.ews.util.*"
%><?xml version="1.0" encoding="UTF-8" ?>  
<%
ServletInputStream in = null;
java.io.BufferedInputStream bin = null;
FrameworkContext fc = FrameworkContext.getContext();
try {
	fc.setWebInfo(request, response, null, null);

	long beforeServletCall = System.currentTimeMillis();
	in = request.getInputStream();
	bin = new java.io.BufferedInputStream( in );
	String args = StreamUtil.getString( bin , "UTF-8" );
	String classURI = "";
	if( args == null || args.equals("") ) {
		WARNINGMsg w = new WARNINGMsg();
		w.msg = "인자가 부족합니다.";
		w.level = Warning.WARNING;
		w.detail = "인자가 부족합니다.";
		out.println( w.toString() );
		return;
	}

	Document doc = XMLUtil.getDocument( args );
	String task = XMLUtil.getTask( doc );
	String action = XMLUtil.getAction( doc );
	String pid = XMLUtil.getAttribute(doc, "PID");
	
	Logger.info( "callServletService.jsp", "callServletService.jsp", "callServletService.jsp task:" + task + " action:" + action);
	Logger.fine( "ARGS : " + args );

	Document retdoc = TaskUtil.execute( doc );
	if( retdoc != null ) {
		long afterServletCall = System.currentTimeMillis();
		XMLUtil.setAttribute( retdoc, "beforeServletCall", beforeServletCall+"");
		XMLUtil.setAttribute( retdoc, "afterServletCall", afterServletCall+"");
		String ret = XMLUtil.indent( retdoc );
		if( ret.length() == 0 ) {
			out.println( "<ret/>" );
			Logger.severe( "callServletService.jsp", "callServletService.jsp", "callServletService.jsp return string is blank");
		} else {
			out.println( ret );
			if( ret.length() > 100 ) {
				Logger.fine( ret.substring(0,100) + "..." );
			} else {
				Logger.fine( ret );
			}
		}
	} else {
		out.println( "<ret/>" );
		Logger.severe( "callServletService.jsp", "callServletService.jsp", "callServletService.jsp return is null");
	}
	return;
} catch (Warning w) {
	Logger.exception("callServletService.jsp", "", "Warning.", w);
	System.out.println( w.getMessage() );
	Document wDoc = XMLUtil.getDocument(w.getMessage());
	String str = XMLUtil.getString(wDoc, "detail");
//	out.println( w.getMessage() );
	if (str.indexOf("XDA 등록정보를 확인하세요") >= 0 && str.indexOf("ORA-") >= 0) {
		XMLUtil.setString(wDoc, "msg", "DB수행 작업시 에러가 발생하였습니다.");
	}
	XMLUtil.setResult(wDoc, -1);
	out.println( XMLUtil.serialize(wDoc) );
} catch (Throwable e) {
	Logger.exception("callServletService.jsp", "", "Exception.", e);
	WARNINGMsg w = new WARNINGMsg();
	w.msg = e.getMessage();
	if( w.msg == null || w.msg.trim().equals("") ) {
		w.msg = "Exception이 발생하였습니다.";
	}
	w.level = Warning.ERROR;
	w.detail = SystemUtil.getStackTrace( e );
	System.out.println( w.toString() );
	Document wDoc = XMLUtil.getDocument(w.toString());
	XMLUtil.setResult(wDoc, -1);
	out.println( XMLUtil.serialize(wDoc) );
//	out.println( w.toString() );
} finally {
	if( bin != null ) {
		try {
			bin.close();
			bin = null;
		} catch( Exception e ) {}
	}
	if( in != null ) {
		try {
			in.close();
			in = null;
		} catch( Exception e ) {}
	}
	fc.setWebInfo(null, null, null, null);
}%>