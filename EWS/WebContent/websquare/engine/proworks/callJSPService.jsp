﻿<%@ page contentType="text/xml; charset=UTF-8" language="java" errorPage="" import="com.inswave.admin.*,com.inswave.system.*,com.inswave.system.exception.*,com.inswave.util.*,com.inswave.msg.warn.*,org.w3c.dom.*"
%><?xml version="1.0" encoding="UTF-8" ?>
<%
ServletInputStream in = null;
java.io.BufferedInputStream bin = null;
try {
	long beforeServletCall = System.currentTimeMillis();	
	in = request.getInputStream();
	bin = new java.io.BufferedInputStream( in );
	String args = StreamUtil.getString( bin , "UTF-8" );
	String classURI = "";
	if( args == null || args.equals("") ) {
		WARNINGMsg w = new WARNINGMsg();
		w.msg = "인자가 부족합니다.";
		w.level = Warning.WARNING;
		w.detail = "인자가 부족합니다.";
		out.println( w.toString() );
		return;
	}
	int idx = args.indexOf(" ");
	if( idx > 0 ) {
		classURI = args.substring( 0, idx );
		args = args.substring( idx + 1 );
		System.out.println("URI[" + classURI + "] args[" + args + "]");
	} else {
		WARNINGMsg w = new WARNINGMsg();
		w.msg = "인자가 부족합니다.";
		w.level = Warning.WARNING;
		w.detail = "인자가 부족합니다.";
		out.println( w.toString() );
		return;
	}

	Document doc = XMLUtil.getDocument( args );
	XMLUtil.setTask( doc, classURI );
	String action = XMLUtil.getAction( doc );
	Logger.fine( "callJSPService.jsp", "callJSPService.jsp", "callJSPService.jsp task:" + classURI + " action:" + action);
	Logger.fine( "ARGS : " + args );

	Document retdoc = TaskUtil.perform( doc );
	if( retdoc != null ) {
		long afterServletCall = System.currentTimeMillis();
		XMLUtil.setAttribute( retdoc, "beforeServletCall", beforeServletCall+"");
		XMLUtil.setAttribute( retdoc, "afterServletCall", afterServletCall+"");
		String ret = XMLUtil.indent( retdoc );
		if( ret.length() == 0 ) {
			out.println( "<ret/>" );
			Logger.severe( "callJSPService.jsp", "callJSPService.jsp", "callJSPService.jsp return string is blank");
		} else {
			out.println( ret );
			if( ret.length() > 100 ) {
				Logger.fine( ret.substring(0,100) + "..." );
			} else {
				Logger.fine( ret );
			}
		}
	} else {
		out.println( "<ret/>" );
		Logger.severe( "callJSPService.jsp", "callJSPService.jsp", "callJSPService.jsp return is null");
	}
	return;
} catch (Warning w) {
	Logger.exception("callJSPService.jsp", "", "Warning.", w);
	System.out.println( w.getMessage() );
	out.println( w.getMessage() );
} catch (Throwable e) {
	Logger.exception("callJSPService.jsp", "", "Exception.", e);
	WARNINGMsg w = new WARNINGMsg();
	w.msg = e.getMessage();
	if( w.msg == null || w.msg.trim().equals("") ) {
		w.msg = "Exception이 발생하였습니다.";
	}
	w.level = Warning.ERROR;
	w.detail = SystemUtil.getStackTrace( e );
	System.out.println( w.toString() );
	out.println( w.toString() );
} finally {
	if( bin != null ) {
		try {
			bin.close();
			bin = null;
		} catch( Exception e ) {}
	}
	if( in != null ) {
		try {
			in.close();
			in = null;
		} catch( Exception e ) {}
	}
}%>