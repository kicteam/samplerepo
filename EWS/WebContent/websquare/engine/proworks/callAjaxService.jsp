<%@ page contentType="text/xml; charset=UTF-8" language="java" errorPage="" 
	import="java.io.*,
			java.net.*,
			java.util.*,
			com.inswave.admin.*,
			com.inswave.system.*,
			com.inswave.system.exception.*,
			com.inswave.util.*,
			com.inswave.msg.warn.*,
			org.w3c.dom.*,
			com.inswave.system.context.FrameworkContext,
			com.ews.util.*"  
%>
<%
ServletOutputStream wout = null;
BufferedOutputStream wbout = null;
ServletInputStream in = null;
BufferedInputStream bin = null;
FrameworkContext fc = FrameworkContext.getContext();

try {
	fc.setWebInfo(request, response, null, null);

	long beforeServletCall = System.currentTimeMillis();
	in = request.getInputStream();
	bin = new BufferedInputStream( in );
	String args = StreamUtil.getString( bin , "UTF-8" );
	String classURI = "";
	if( args == null || args.equals("") ) {
		WARNINGMsg w = new WARNINGMsg();
		w.msg = "인자가 부족합니다.";
		w.level = Warning.WARNING;
		w.detail = "인자가 부족합니다.";
		out.println( w.toString() );
		return;
	}

	Document doc = XMLUtil.getDocument( args );
	String task = XMLUtil.getTask( doc );
	String action = XMLUtil.getAction( doc );
	String pid = XMLUtil.getAttribute(doc, "PID");
	
	Logger.info( "callAjaxService.jsp", "callAjaxService.jsp", "callAjaxService.jsp task:" + task + " action:" + action);
	Logger.fine( "ARGS : " + args );

	Document retdoc = TaskUtil.execute( doc );
	long afterServletCall = System.currentTimeMillis();
	XMLUtil.setAttribute( retdoc, "beforeServletCall", beforeServletCall+"");
	XMLUtil.setAttribute( retdoc, "afterServletCall", afterServletCall+"");
	String ret = XMLUtil.indent( retdoc );
	wout = response.getOutputStream();
	wbout = new BufferedOutputStream(wout);    
	byte[] b = ret.getBytes("UTF-8");
	response.setContentType("text/xml;charset=utf-8");
	wbout.write( b, 0, b.length );
	return;
} catch( Exception e ) {
	/*
} catch (Warning w) {
	Logger.exception("callAjaxService.jsp", "", "Warning.", w);
	System.out.println( w.getMessage() );
	Document wDoc = XMLUtil.getDocument(w.getMessage());
	String str = XMLUtil.getString(wDoc, "detail");
//	out.println( w.getMessage() );
	if (str.indexOf("XDA 등록정보를 확인하세요") >= 0 && str.indexOf("ORA-") >= 0) {
		XMLUtil.setString(wDoc, "msg", "DB수행 작업시 에러가 발생하였습니다.");
	}
	XMLUtil.setResult(wDoc, -1);
	out.println( XMLUtil.serialize(wDoc) );
} catch (Throwable e) {
	e.printStackTrace();
	Logger.exception("callAjaxService.jsp", "", "Exception.", e);
	WARNINGMsg w = new WARNINGMsg();
	w.msg = e.getMessage();
	if( w.msg == null || w.msg.trim().equals("") ) {
		w.msg = "Exception이 발생하였습니다.";
	}
	w.level = Warning.ERROR;
	w.detail = SystemUtil.getStackTrace( e );
	System.out.println( w.toString() );
	Document wDoc = XMLUtil.getDocument(w.toString());
	XMLUtil.setResult(wDoc, -1);
	out.println( XMLUtil.serialize(wDoc) );
//	out.println( w.toString() );
*/
} finally {
	if( wout != null ) try { wout.flush(); } catch (Exception e) {}
	if( wbout != null ) try { wbout.close(); } catch (Exception e) {}
	if( bin != null ) try { bin.close(); } catch (Exception e) {}
	if( in != null ) try { in.close(); } catch (Exception e) {}
}
	fc.setWebInfo(null, null, null, null);
%>