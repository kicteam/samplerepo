<%@ page contentType="text/xml; charset=utf-8" language="java" errorPage="" import="com.inswave.admin.*,com.inswave.util.*,com.inswave.system.config.*,java.util.*,org.w3c.dom.*"
%><?xml version="1.0" encoding="utf-8" ?>
<%
try {
	XMLConfiguration conf = XMLConfiguration.getInstance();
	Vector vec = conf.getChildren( "/proworks/clientConfiguration" );
	Document doc = XMLUtil.getDocument("<ClientConfiguration/>");
	for( int i = 0 ; i < vec.size() ; i++ ) {
		Hashtable hash = (Hashtable)vec.elementAt(i);
		String key = (String)hash.get("key");
		String value = (String)hash.get("value");
		XMLUtil.setString( doc, key, value );
	}
	System.out.println( "configuration.jsp ==> " + XMLUtil.indent( doc ) );
	out.println( XMLUtil.indent( doc ) );
} catch (Exception e) {
	Logger.exception("configuraiton.jsp", "", "Exception.", e);
	out.println( "<ClientConfiguration/>" );
}
%>