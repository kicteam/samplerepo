<%@ page contentType="text/xml; charset=euc-kr" language="java" errorPage="" import="java.util.*,com.inswave.admin.*,com.inswave.util.*"
%><?xml version="1.0" encoding="EUC-KR"?>
<TOLBA01>
<%
try {
	String pattern = com.inswave.util.ServletUtil.getParameter( request, "pattern" );
	String offset = com.inswave.util.ServletUtil.getParameter( request, "offset" );
	String offsetType = com.inswave.util.ServletUtil.getParameter( request, "offsetType" );
	String date = null;
	if( offset == null || offset.equals( "" ) ) {
		if ( pattern == null || pattern.equals( "" ) ) {
			pattern = "";
			date = String.valueOf( System.currentTimeMillis() );
		} else {
			date = DateUtil.getCurrentDate( pattern );
		}
	} else {
		int iOffset = Integer.parseInt( offset );
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(pattern, Locale.KOREA);
		Calendar cal = Calendar.getInstance();
		if( offsetType != null && offsetType.equals("M") ) {
			cal.add(Calendar.MONTH, iOffset);
		} else {
			offsetType = "D";
			cal.add(Calendar.DATE, iOffset);
		}
		date = formatter.format(cal.getTime());
		out.println( "<offset value='" + offset + "'/>" );
		out.println( "<offsetType value='" + offsetType + "'/>" );
	}
	out.println( "<pattern value='" + pattern + "'/>" );
	out.println( "<date value='" + date + "'/>" );
} catch (Exception e) {
	Logger.exception("currentTime.jsp", "", "Exception.", e);
	throw e;
}
%>
</TOLBA01>