<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ page import = "java.io.*, java.net.*, java.sql.*, java.util.*"%>
<%
	String cr_date = "";
	String emp_id = "";
	String seq = "";

	String strHead = "";
	String strMessage = "";
	String strEnd = "";
	
	Connection conn = null ;
	Statement stmt = null ;
	ResultSet rset = null ;
	
	try {
		cr_date = request.getParameter("CR_DATE") != null ? request.getParameter("CR_DATE").toString() : "";	//"20120130";
		emp_id = request.getParameter("EMP_ID") != null ? request.getParameter("EMP_ID").toString() : "";	//"sklee";
		seq = request.getParameter("SEQ") != null ? request.getParameter("SEQ").toString() : "";	//"1";
		
		String db_URL = "jdbc:oracle:thin:@192.168.40.114:1521:EWSTEST";
		String db_ID = "EWS";
		String db_PWD = "password1";
		
		String SERVER_IP = java.net.InetAddress.getLocalHost().getHostAddress();
		
		if (SERVER_IP.equals("192.168.20.170")) {   
			//db_URL = "jdbc:oracle:thin:@192.168.20.172:1521:ERMPRD";
			db_URL = "jdbc:oracle:thin:@192.168.21.85:3001:ERMPRD";
			db_ID = "EWS";
			db_PWD = "nevrisk0$";
		} 
		
		db_URL = "jdbc:oracle:thin:@192.168.21.85:3001:ERMPRD";
		db_ID = "EWS";
		db_PWD = "nevrisk0$";
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");    
			conn = DriverManager.getConnection(db_URL,db_ID,db_PWD);
			
			stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE );
			
			String query = " select html_head, message, html_end "
						+  " from ews_mail "
						+  " where cr_date = '" + cr_date + "' "
						+  " and emp_id = '" + emp_id + "' "
						+  " and seq = " + seq;
	
			rset = stmt.executeQuery(query);
			
			if(rset.next()) {
				StringBuffer output = new StringBuffer();
				Reader input = rset.getCharacterStream("html_head");
				char[] buffer = new char[1024];
				int byteRead;
				while((byteRead=input.read(buffer,0,1024))!=-1){
					output.append(buffer,0,byteRead);
				}
				input.close();
				strHead = output.toString();
				
				output = new StringBuffer();
				input = rset.getCharacterStream("message");
				while((byteRead=input.read(buffer,0,1024))!=-1){
					output.append(buffer,0,byteRead);
				}
				input.close();
				strMessage = output.toString();
				
				output = new StringBuffer();
				input = rset.getCharacterStream("html_end");
				while((byteRead=input.read(buffer,0,1024))!=-1){
					output.append(buffer,0,byteRead);
				}
				
				input.close();
				strEnd = output.toString();
							
			} else {
				throw new Exception("");
			} 	
		} catch(Exception e) {
			throw new Exception("");
		} finally {
			rset.close();     
			stmt.close();
			conn.close();
		}
	} catch (Exception e) {
		out.println(e.toString());
		%>
		<script language="javascript">
			alert("메일 발송내역이 존재하지 않습니다.");
			window.close();
		</script>
		<%
	}

	strHead = strHead.replace("&lt;", "<").replace("&gt;", ">").replace("&quot;", "'");
	strMessage = strMessage.replace("&lt;", "<").replace("&gt;", ">").replace("&quot;", "'");
	strEnd = strEnd.replace("&lt;", "<").replace("&gt;", ">").replace("&quot;", "'");
	
	out.println(strHead);
	out.println(strMessage);
	out.println(strEnd);
%>
