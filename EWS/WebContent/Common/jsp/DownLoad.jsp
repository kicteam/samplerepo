<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import ="java.io.File"%>
<%@ page import ="java.io.BufferedInputStream"%>
<%@ page import ="java.io.BufferedOutputStream"%>
<%@ page import ="java.io.FileInputStream"%>
<%@ page import ="java.net.URLEncoder"%>
<%@ page import ="org.w3c.dom.Document"%>
<%@ page import = "java.util.*"%>
<%@ page import = "com.inswave.admin.*"%>
<%@ page import = "com.inswave.system.*"%>
<%@ page import = "com.inswave.system.exception.*"%>
<%@ page import = "com.inswave.msg.warn.*"%>
<%@ page import = "com.inswave.util.*"%>
<%@ page import = "com.inswave.util.XMLUtil"%>
<%
	System.out.println("download.jsp :: start");

try{
	String key = "";
	String[] value = null;
	Map parameterMap = request.getParameterMap();
	Iterator itr = parameterMap.keySet().iterator();
	
	while (itr.hasNext()) {
		key = (String) itr.next();
		value = (String[]) parameterMap.get(key);
		if (value != null && value.length > 0) {
			System.out.println("key == " + key + "  value == " + value[0]);
			
			String strDoc = value[0];
			Document doc = XMLUtil.parse(strDoc);
			String flRname = XMLUtil.getNodeValue(doc, "data/RESULT/FL_RNAME");
			String flSname = XMLUtil.getNodeValue(doc, "data/RESULT/FL_SNAME");
			String flPath = XMLUtil.getNodeValue(doc, "data/RESULT/FL_PATH");
			String flType = XMLUtil.getNodeValue(doc, "data/RESULT/FL_TYPE");
			
			String createFilePath =  request.getRealPath("/")+"upload\\";
			String fullFileName  = createFilePath+flPath+"/"+flSname+"."+flType;	// 서버 다운로드 파일
			String userFileName = flRname+"."+flType;	// 로컬 다운로드될 파일명
			
			response.resetBuffer();
			response.setContentType("application/octet-stream; charset=euc-kr;");
			response.setHeader("Content-Transfer-Encoding", "binary");
			response.setHeader("Pragma", "no-cache;"); 
			response.setHeader("Expires", "-1;");

			String userAgent = request.getHeader("User-Agent");
			if(userAgent != null && userAgent.indexOf("MSIE") > -1){
				// ms ie 6.x 이상
				response.setHeader("Content-Disposition", "attachment; filename=" + new String(userFileName.getBytes("euc-kr"), "latin1") + ";");
			}else{
				// 모질라, 오페라
				 response.setHeader("Content-Disposition", "attachment; filename="+ new String(userFileName.getBytes("euc-kr"), "latin1") + ";");
			}

			File file = new File(fullFileName);
			byte b[] = new byte[(int)file.length()];

			if(file.length()>0){
				//response.setContentLength(file.length());
				response.setHeader("Content-Length", ""+file.length());
			}
			
			if (file.isFile()){
				BufferedInputStream fin = new BufferedInputStream(new FileInputStream(file));
				BufferedOutputStream outs = new BufferedOutputStream(response.getOutputStream());
				int read = 0;
				while ((read = fin.read(b)) != -1){
					outs.write(b,0,read);
				}
				outs.close();
				fin.close();
			}
		}
	}
}catch(Exception e){
	throw e ;
}
%>