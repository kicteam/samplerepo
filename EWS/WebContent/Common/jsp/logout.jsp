<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="" import="com.inswave.admin.*,com.inswave.system.*,com.inswave.system.exception.*,com.inswave.util.*,com.inswave.msg.warn.*,org.w3c.dom.*,com.inswave.system.context.*,com.ews.util.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns='http://www.w3.org/1999/xhtml' xmlns:ev='http://www.w3.org/2001/xml-events' xmlns:w2='http://www.inswave.com/websquare' xmlns:xf='http://www.w3.org/2002/xforms'>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<META http-equiv="X-UA-Compatible" content="IE=Edge"/>
	<title>	WebSquare </title>
	<script type="text/javascript" src="javascript.wq?q=/bootloader"></script>
	<script type="text/javascript" src="/EWS/Common/js/common.js"></script>
<%
	FrameworkContext fc = FrameworkContext.getContext();
	fc.setWebInfo(request, response, null, null);
	
	EwsLoginUtil.removeSession();
	EwsLoginInfoBean userInfo = (EwsLoginInfoBean) EwsLoginUtil.getSessionInfo();
	if (userInfo != null) {
		Logger.info("____"+XMLUtil.indent(userInfo.readUserDoc()));
%>
	<script type="text/javascript" language="javascript">
		document.location.href = collLogoutPath;
	</script>
<%
	}
%>
	<script type="text/javascript" language="javascript">
		/*로그아웃*/
		WebSquare.session.removeSession();
		//document.location.href = contextPath+"?MENU_ID=00001";
		document.location.href = "http://ews.s-oil.com:8002/EWS/";
	</script>
</head>
<body></body>
</html>
<%	
	fc.setWebInfo(null, null, null, null);
%>