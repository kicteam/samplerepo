<%@ page language="java" contentType="text/html; charset=utf-8"%>
<%@ page import="java.io.*, java.text.*, java.lang.*, java.util.*, java.net.*, java.sql.*"%>
<%@ page import="SOIL.DB.*, SOIL.util.*" %>
<%@ page import="SOIL.ews.excel.*" %>
<%@ page import="SOIL.DB.Query.CallQuery" %><%
	request.setCharacterEncoding("utf-8");
	
	String rpScenario = StringUtility.convertNull(request.getParameter("KRI_ID"),"");
	String rpDateGb = StringUtility.convertNull(request.getParameter("TYPE"),"");	// 'ETL':EWS가동일, 'NOETL':발생일
	String rpDateFrom = StringUtility.convertNull(request.getParameter("ST_DT"),"");
	String rpDateTo = StringUtility.convertNull(request.getParameter("ED_DT"),"");
	
	String filePath = request.getRealPath("/") + "excelfiles/";
	String client = request.getHeader("user-agent"); 

	if(rpDateGb.equals("ETL")) rpDateGb = "EWS";
	else rpDateGb = "CRT";
	
	ReportQuery aReportQuery = new ReportQuery();
	
	String[] result = new String[4];
	result[0] = "";
    result[1] = "";
	result[2] = "F";
	result[3] = "";
		
	int iRecord = 0;
	try {
		
		String page_info = "";
		
		
		Hashtable hash = aReportQuery.getReport(rpScenario, rpDateGb, rpDateFrom, rpDateTo);
		
		
		
		
		String query = (String)hash.get("query");
		String err_code = (String)hash.get("err_code");			
		String err_message = (String)hash.get("err_message");
		String db_message = err_message;
		String[] cursor_info = (String[])hash.get("cursor_info");	
		List rsList = (List)hash.get("rsList");
				
		if(rsList.size() == 0 || err_code.equals("E")) {
			err_code = "F";
			err_message = "정보가 존재하지 않습니다.";
		} 
		iRecord = 	rsList.size();
		
		if(rsList.size() > 5000) {
			result = aReportQuery.makeText(filePath, rpScenario, rpDateGb, rpDateFrom, rpDateTo, hash);
		} else {
			%><script language='javascript'>alert("데이터량이 5000건 이하 입니다. 'EXCEL 다운로드'를 이용해 주세요.");history.back();</script><%
		}
	} catch (Exception e) {
		
	}
	 
	if(result[2].equals("S")) {
		PrintStream printstream = new PrintStream(response.getOutputStream(), true);
				
		try {

			File file = new File(filePath + result[1]); //파일 전체 경로
			FileInputStream fin = new FileInputStream(file);

			int ifilesize = (int)file.length();
			byte b[] = new byte[ifilesize];	  
			
			/*
			response.setContentLength(ifilesize);
			response.setContentType("application/smnet;charset=euc-kr"); 
			response.setHeader("Accept-Ranges", "bytes");	
			*/
			
			if(client.indexOf("MSIE 5.5")>-1) {
				response.setHeader("Content-Disposition", "filename=" + result[1] + ";");
			} else {
				response.setHeader("Content-Disposition", "attachment;filename=" + result[1] + ";");
			}
			
			ServletOutputStream oout = response.getOutputStream();	    

			fin.read(b);
			oout.write(b,0,ifilesize);
			oout.close();
			fin.close();
			
		} catch(Exception e) { 
			
		}
	} else {
		if(iRecord == 0) {
		%><script language='javascript'>alert("data not exist");history.back();</script><%
		}
	}
	
%>