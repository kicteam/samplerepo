//-----------------------------------------------------------------------------
// 공통메세지를 가져온다. - 차후 properties에서 관리하여 읽어올 예정
// msgClsf("q":question, "i":information, "r":request)
// msgId  ("save","insert","update","delete","approval")
// word1($1 변경할 문자)
// word2($2 변경할 문자)
//-----------------------------------------------------------------------------
function gfnGetMsg() {
	var g_msgJson = {
		// submission processMsg
		"p":{
				"retrieve"	:"[조회] 중입니다.",
				"save"		:"[저장] 중입니다.",
				"insert"	:"[등록] 중입니다.",
				"update"	:"[수정] 중입니다.",
				"delete"	:"[삭제] 중입니다.",
				"process"	:"[처리] 중입니다."
			}
		,
		"q":{
				"save"    	:"[저장] 하시겠습니까?",
				"insert"	:"[등록] 하시겠습니까?",
				"update"  	:"[수정] 하시겠습니까?",
				"delete"  	:"[삭제] 하시겠습니까?",
				"process" 	:"[처리] 하시겠습니까?",
				"approval"	:"[결재] 하시겠습니까?",
				"other"		:"[$1] 하시겠습니까?"
			}
		,
		"i":{
				"select"	:"[조회] 되었습니다.",
				"selectNo"	:"[조회]된 자료가 없습니다.",
				"save"		:"[저장] 하였습니다.",
				"insert"	:"[등록] 하였습니다.",
				"update"	:"[수정] 하였습니다.",
				"delete"	:"[삭제] 하였습니다.",
				"process"	:"[$1] 하였습니다.",
				"processNo"	:"[$1] 할 자료가 없습니다.",
				"updateNo"	: "[등록,수정,삭제]된 자료가 없습니다."
			}
		,
		"r":{
				"select" :"$1를(을) 선택하세요.",
				"insert" :"$1를(을) 입력하세요.",
				"preAddChk1":"저장후 추가작업 하세요."
			}
		,
		"g":{
				"row" : "$1할 [row]가 없습니다.",
				"code": "Missing CodeType",
				"noPage": "페이지 URL이 존재하지 않거나 잘못된 접근입니다.\n 관리자에게 문의하십시요!",
				"noSession": "로그인 정보가 존재하지 않습니다. 로그인 후 사용하세요!",
				"noSsoLogin": "로그인 정보가 존재하지 않습니다. 관리자에게 문의하십시요!",
				"noPassword": "[USERID] 또는 [PASSWORD]가 잘못 되었습니다.\n[eoffice]를 통해 로그인하세요!",
				"noUserId": "[USERID] 또는 [PASSWORD]가 잘못 되었습니다.\n[eoffice]를 통해 로그인하세요!",
				"noTopMenu": "[메뉴]에 대한 권한이 없습니다.\n 관리자에게 문의하십시요!",
				"noFileDelete": "[삭제] 할 파일이 없습니다.",
				"noDeptId": "사용할수 없는 [$1] 입니다.",
				"okDeptId": "사용가능한 [$1] 입니다.",
				"noRoot": "[$1]은 삭제할수 없습니다.",
				"noCheck": "중복체크를 하십시요!",
				"noApprId": "승인자가 지정되어 있지 않습니다.",
				"notEngNum": "[ID] 또는 [PASSWORD]는 \n[영문소문자] 또는 [숫자]만 사용할수 있습니다.",
				"noChildNodes": "[하위부서]가 있을시 부서이동을 하실수 없습니다."
			}
	};
	var argLen = arguments.length;
	if(argLen < 2) return "Message Not Found.";
	var msg = g_msgJson[arguments[0]][arguments[1]];

	if(argLen == 2) {
		return msg;
	} else if(argLen == 3) {
		return msg.replace(/[$]1/,arguments[2]);
	} else if(argLen == 4) {
		return msg.replace(/[$]1/,arguments[2]).replace(/[$]2/,arguments[3]);
	}
	return msg;
}