/**
 * Submission 함수 호출.
 * @param submissionID : 서브미션 ID(String)      
 * @example gfnExecuteSubmission("select");       
 */				
function gfnExecuteSubmission(submissionID) {
	if (submissionID.trim().length < 1) {
		return;
	}

	try {
		var submission = WebSquare.ModelUtil.getSubmission(submissionID);
		var insAction =  WebSquare.ModelUtil.getInstanceValue( submission.ref+"/@action" );
		if(insAction.isEmpty()) {
			var msg = submission.processMsg;
			if (msg > ' ') {
				submission.processMsg = gfnGetMsg("p", "process");	// 처리중입니다.
			}
		}else{
			submission.processMsg = gfnGetMsg("p", insAction);
			switch(insAction) {
				case "save":
				case "insert":
				case "update":
				case "delete":
				case "process":
					if(!ewsMsg.gfnConfirm("q", insAction)){
						return false;
					}
					break;
				//default :
//					break;
			}
		}
		submission.action = collPath;
		WebSquare.ModelUtil.executeSubmission(submissionID);
	}catch (e){
		WebSquare.exception.printStackTrace(e);
	}
}

/**
 * File Submission 함수 호출.
 * @param submissionID : 서브미션 ID(String)      
 * @example gfnFileSubmission("select");       
 */				
function gfnFileSubmission(submissionID) {
	if (submissionID.trim().length < 1) {
		return;
	}

	try {
		var submission = WebSquare.ModelUtil.getSubmission(submissionID);
		var insAction =  WebSquare.ModelUtil.getInstanceValue( submission.ref+"/@action" );
		if(insAction.isEmpty()) {
			var msg = submission.processMsg;
			if (msg > ' ') {
				submission.processMsg = gfnGetMsg("p", "process");	// 처리중입니다.
			}
		}else{
			submission.processMsg = gfnGetMsg("p", insAction);
		}
		submission.action = callFilePath;
		WebSquare.ModelUtil.executeSubmission(submissionID);
	}catch (e){
		WebSquare.exception.printStackTrace(e);
	}
}

/**
 * Submission 후 함수호출.
 * @param submissionID : 서브미션 ID(String)      
 * @example callbackSubmission(e);       
 */	
function callbackSubmission(e,nm){
	try {
		var warnMsg = WebSquare.xml.findNode(e.responseBody, "WARNING");
		if (warnMsg != null) {
			var errMsg = WebSquare.xml.getValue(warnMsg, "msg", "value");
			var errLevel = WebSquare.xml.getValue(warnMsg, "level", "value");
			var errDetail = WebSquare.xml.getValue(warnMsg, "detail", "value");
			var errorCode = WebSquare.xml.getValue(warnMsg, "errorCode", "value");
			var screenID = WebSquare.xml.getValue(warnMsg, "screenID", "value");
			var errorMessage = "errLevel : "+ errLevel;
			alert( errorMessage );
			return false;
		}else{
			var totCnt = WebSquare.xml.getValue( e.responseBody , "vector", "totalRows" );
			// totalRows 가 있을시에만
			if(totCnt>0){
				var totPage = totCnt/ WebSquare.util.parseInt( pageSize, 0 );
				var pageListAry = WebSquare.WebSquareDocument.getElementsByTagName("pageList");

				if( totCnt % WebSquare.util.parseInt( pageSize, 0 )>0 ) totPage++;
				
				if(nm==null) nm = 0;
				pageListAry[nm].setCount(totPage);
				pageListAry[nm].setSelectedIndex(currPage);
			}
			return e.responseBody;
		}
	}catch (e){
		WebSquare.exception.printStackTrace(e);
	}
}