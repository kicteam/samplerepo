imports('/EWS/Common/js/messages.js');

var contextPath = "/EWS/websquare/websquare.jsp";
var fileDownPath = "/EWS/Common/jsp/DownLoad.jsp";
var popupPath = "/EWS/websquare/popup.jsp";
var collPath 	= "/EWS/websquare/engine/proworks/callServletService.jsp";
var collAjaxPath = "/EWS/websquare/engine/proworks/callAjaxService.jsp";
var collFilePath= "/EWS/websquare/engine/proworks/callDownFileService.jsp";
var collLogoutPath = "/EWS/Common/jsp/logout.jsp";
var pageSize = "1000";	// page 관련[한페이지 사이즈]
var currPage = "1";		// page 관련[현제 페이지]
var topYn = true;		// 로그인시 메뉴조회

/* param */
var KRI_AUTH = WebSquare.net.getParameter("KRI_AUTH"); // 시나리오 관련
var vKRI_RANK = WebSquare.net.getParameter("KRI_RANK"); // 시나리오 관련
var vKRI_ID = WebSquare.net.getParameter("KRI_ID"); // 시나리오 관련
var vKRI_NM = WebSquare.session.getAttribute( "KRI_NM"); // 한글 파라메타 편법.

var EwsMsg				= function(){};

/**
 * 앞뒤 공백 제거 함수
 */
String.prototype.trim = function() {
	return this.replace(/(^\s*)|(\s*$)/g, "");
};

/**
 * 숫자 뒤에 px 붙이기 
 */
String.prototype.px = function() {
	return Math.round(this) + "px";
};

/**
 * 문자 맨 앞의 공백문자를 제거하는 함수
 */
String.prototype.firstEnc = function() {
	var str = this + "";
	var len = str.length;
	var i = 0;
	for(i = 0; (str.charAt(i) == ' '||(str.charAt(i)=="\n" )||(str.charAt(i)=="\r" )) && i < len; i++);
	str = str.substring(i, len);
	return str;
};

/**
 * 문자 뒤의 공백문자를 제거하는 함수
 */
String.prototype.endEnc = function() {
	var str = this + "";
	var len = str.length;
	for(var i = (len - 1); (str.charAt(i) == ' '||(str.charAt(i)=="\n" )||(str.charAt(i)=="\r" )) && i > 0; i--);
	str = str.substring(0, i + 1);
	return str;
};

/**
 * 문자열을 쿼테이션으로 감싼다.
 */
String.prototype.quote = function() {
	return "'" + this + "'";
};

/**
 * String 치환
 */
String.prototype.replaceAll = function(findValue, replaceValue) {
	return this.replace(new RegExp(findValue,"g"), replaceValue);
};

/**
 * String 인지 아닌지 체크 
 */
function isStrty(val){
	return WebSquare.util.isString( val );
};

/**
 * 문자열이 비어 있는가를 체크
 */
String.prototype.isEmpty = function() {
	return (this == null || this.trim() == "" || this == undefined || this == 'undefined') ? true : false;
};

/**
 * 오브젝트가 비어 있는가를 체크하는 함수 
 */
function isBlank(objj) {
	return WebSquare.util.isNull( objj );
};

/**
 * 대상이 String인제 체크 하여 비교해줌
 * @return true[없을때]:flase[있을때]
 */
function isNotNull(obj){
	if( isStrty(obj) ){
		return obj.isEmpty();
	}else {
		return isBlank(obj);
	}
}

/**
 * 메일의 유효성을 체크.
 * @return : true | false
 */
String.prototype.isMail = function() {
	var em = this.trim().match(/^[_\-\.0-9a-zA-Z]{3,}@[-.0-9a-zA-z]{2,}\.[a-zA-Z]{2,4}$/);
	return (em) ? true : false;
};

/**
 * 숫자만 체크
 * @return : true | false
 */
String.prototype.num = function() {
	return (this.trim().match(/^[0-9]+$/)) ? true : false;
};

/**
 * 영어만 체크
 * @return : true | false
 */
String.prototype.isEng = function() {
	return (this.trim().match(/^[a-zA-Z]+$/)) ? true : false;
};

/**
 * 영어,숫자
 * @return : true | false
 */
String.prototype.isEngNum = function() {
	return (this.trim().match(/^[0-9a-zA-Z]+$/)) ? true : false;
};

/**
 * 소문자 영어,숫자
 * @return : true | false
 */
String.prototype.isNotEngNum = function() {
	return (this.trim().match(/^[0-9a-z]+$/)) ? true : false;
};

/**
 * 한글만 체크
 * @return : true | false
 */
String.prototype.isKor = function() {
	return (this.trim().match(/^[가-힣]+$/)) ? true : false;
};

/**
 * 숫자와 . - 이외의 문자는 다 뺀다. - 통화량을 숫자로 변환
 * @return : 숫자,-
 */
String.prototype.toNum = function() {
	var num = this.trim();
	return (this.trim().replace(/[^0-9\.-]/g,""));
};

/**
 * 숫자 이외에는 다 뺀다.
 * @return : 숫자
 */
String.prototype.onlyNum = function() {
	var num = this.trim();
	return (this.trim().replace(/[^0-9]/g,""));
};

/**
 * 숫자만 뺀 나머지 전부
 * @return : 숫자 이외
 */
String.prototype.noNum = function() {
	var num = this.trim();
	return (this.trim().replace(/[0-9]/g,""));
};

/**
 * 숫자에 3자리마다 , 를 찍어서 반환
 * @return : 통화량
 */
String.prototype.toMoney = function() {
	var num = this.toNum();
	var pattern = /(-?[0-9]+)([0-9]{3})/;
	while(pattern.test(num)) {
		num = num.replace(pattern,"$1,$2");
	}
	return num;
};

/**
 * 파일명 반환
 * @return : String
 */
String.prototype.getFileNm = function() {
	var str = this.substring(this.lastIndexOf(".xml") + 4, this.lastIndexOf("."));
	return str;
};

/**
 * 파일 확장자 반환
 * @return : String
 */
String.prototype.getExt = function() {
	var str = this.substring(this.lastIndexOf(".") + 1, this.length);
	return str;
};

/**
 * Browser 체크
 * @사용법 : Prototype.Browser.IE(IE, Opera, WebKit, Gecko, MobileSafari)
 */
var Prototype = {
	Version: '1.6.0.3',
		Browser: {
			IE: !!(window.attachEvent && navigator.userAgent.indexOf('Opera') === -1),
			Opera: navigator.userAgent.indexOf('Opera') > -1,
			WebKit: navigator.userAgent.indexOf('AppleWebKit/') > -1,
			Gecko: navigator.userAgent.indexOf('Gecko') > -1 && navigator.userAgent.indexOf('KHTML') === -1,
			MobileSafari: !!navigator.userAgent.match(/Apple.*Mobile.*Safari/)
		}
};

/**
 * 주민번호 체크 XXXXXX-XXXXXXX 형태로 체크
 * @return : true(맞는 형식) | false(잘못된 형식)
 */
String.prototype.isJumin = function() {
	var num = this.trim().onlyNum();
	if(num.length == 13) {
		num = num.substring(0, 6) + "-" + num.substring(6, 13);
	}else {
		return false;
	}
	num = num.match(/^([0-9]{6})-?([0-9]{7})$/);
	if(!num) return false;
	var num1 = RegExp.$1;
	var num2 = RegExp.$2;
	if(!num2.substring(0, 1).match(/^[1-4]{1}$/)) return false;
	num = num1 + num2;
	var sum = 0;
	var last = num.charCodeAt(12) - 0x30;
	var bases = "234567892345";
	for (i=0; i<12; i++) {
		sum += (num.charCodeAt(i) - 0x30) * (bases.charCodeAt(i) - 0x30);
	}
	var mod = sum % 11;
	return ((11 - mod) % 10 == last) ? true : false;
};

/**
 * 사업자번호 체크 XXX-XX-XXXXX 형태로 체크
 * @return : true(맞는 형식) | false(잘못된 형식)
 */
String.prototype.isBiznum = function() {
	var checkID = new Array(1, 3, 7, 1, 3, 7, 1, 3, 5, 1);
	var i, Sum=0, c2, remander;
	
	var bizID = this.trim().replace(/-/gi,''); 
	for (i=0; i<=7; i++){
		Sum += checkID[i] * bizID.charAt(i);
	}
	
	c2 = "0" + (checkID[8] * bizID.charAt(8));
	c2 = c2.substring(c2.length - 2, c2.length);
	
	Sum += Math.floor(c2.charAt(0)) + Math.floor(c2.charAt(1));
	remander = (10 - (Sum % 10)) % 10 ;
	
	if(bizID.length != 10){
		   return false;
	} else if (Math.floor(bizID.charAt(9)) != remander){
		   return false;
	} else {
		   return true;
	}
};

/**
 * 핸드폰 체크 XXX-XXXX-XXXX 형태로 체크
 * @return : true | false
 */
String.prototype.isMobile = function() {
	var num = this.trim().onlyNum();
	num = num.substring(0, 3) + "-" + num.substring(3, num.length - 4) + "-" + num.substring(num.length - 4, num.length);
	num = num.trim().match(/^01[016789]{1}-[1-9]{1}[0-9]{2,3}-[0-9]{4}$/);
	return (num) ? true : false;
};

/**
 * SMS관련[S]   추후 손볼거임 sms 관련은 사용하지 마세요.
 * @returns: ErrorCode, num1, num2, num3, hasStar, has200
 *           ErrorCode : 0 ok
 */
function parseNumber( str ) {
	var result = new Array(6);
	
	var len = str.length;
	var num = str;
	var num1, num2, num3;
	var errorCode = 0;
	
	if ( len < 10 || len>11) {
		errorCode = 1;
	} else {
		num1 = num.substring(0,3);
		num2 = num.substring(3,len-4 );
		num3 = num.substring(len-4,len );	

		if ( num1 != "010" && num1 != "011" && num1 != "016" && num1 != "017" && num1 != "018" && num1 != "019" ) {
			errorCode = 2;
		} else {
			if ( !isNumeric(num) ) 	errorCode = 3;
		}
	}
	
	result[0] = errorCode;
	result[1] = num1;
	result[2] = num2;
	result[3] = num3;
	
	return result;
}

/**
 * 번호를 검사하고 - 을 채운다.
 * <Valid 목록>
 *  3자리 국번: 010-222-2222
 *  4자리 국번: 010-2222-2222
 *  200메시지: 200-010-111-1111
 *  별메시지: 010-222-3333*
 */
function addHipen( str, warn ) {
	var parsed = parseNumber( str );
	if ( parsed[0] != 0 ) {
	/*
		if(warn) window.alert( "번호 형식이 잘못되었습니다 - code:" + parsed[0] );
		return "";
	*/	
		if(warn) window.alert( "번호 형식이 잘못되었습니다.");
			return "";
	}
	
	var num1 = parsed[1];
	var num2 = parsed[2];
	var num3 = parsed[3];
	var num = num1 + num2 + num3;
	var len = num.length;
	
	return num1 + "-" + num2 + "-" + num3;	
}

/**
 * 숫자 3자리마다 , 붙인다
 */
function formatNumber( num ) {
	var src = String(num);
	var tmp = src.replace( /(\d)(\d{3}$)/g, "$1,$2");
	var result = tmp.replace( /(\d)(\d{3},)/g, "$1,$2" );
	while ( result != tmp ) {
		tmp = result;
		result = tmp.replace( /(\d)(\d{3},)/g, "$1,$2" );
	}

	return result;
}

function isNumeric( num ) {
	for (var i=3; i<num.length; i++) {
		var ch = num.charAt(i);
		if ( ch < '0' || ch > '9' ) {
			return false;
		}
	}
	return true;
}
/* SMS관련[E] */


/**
 * 메세지 뛰우기
 */
EwsMsg.prototype = {
	/**
	 * 공통 알림창
	 * @param msgBigTyp 타입1
	 * @param msgMidTyp 타입2
	 * @param hashParam 치환할 파라미터
	 */
	gfnAlert : function(msgBigTyp, msgMidTyp, hashParam){
		["msg.gfnAlert"];
		try {
			alert( gfnGetMsg(msgBigTyp, msgMidTyp, hashParam) );
		} catch (e) {
			alert ("ewsMsg.gfnAlert : " + e);
		}
	},

	/*
	 * 공통 확인창
	 * @param msgBigTyp 타입1
	 * @param msgMidTyp 타입2
	 * @param hashParam 치환할 파라미터
	 * @return
	 */
	gfnConfirm : function(msgBigTyp, msgMidTyp, hashParam){
		["msg.gfnAlert"];
		try {
			return confirm( gfnGetMsg(msgBigTyp, msgMidTyp, hashParam) );
		} catch (e) {
			alert ("msg.gfnAlert : " + e);
		}
	}
};

var ewsMsg 				= new EwsMsg();