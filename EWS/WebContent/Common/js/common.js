imports('uiplugin.popup');
imports('/EWS/Common/js/jquery-1.6.2.js');
imports('/EWS/Common/js/json2.js');
imports('/EWS/Common/js/submission.js');
imports('/EWS/Common/js/util.js');


/* 세션 선언[S] */
var ssUserId	= WebSquare.session.getAttribute( "EMP_ID");		// 아이디
var ssUserNm	= WebSquare.session.getAttribute( "EMP_NAME");		// 명
var ssDeptId 	= WebSquare.session.getAttribute( "DEPT_ID");		// 부서ID
var ssSmpDeptNm = WebSquare.session.getAttribute( "SAP_DEPT_NM");	// SMP부서명
var ssAuthId	= WebSquare.session.getAttribute( "AUTH_ID"); 		// 사용자 권한

var adminYn = false;	// 로그인시 메뉴 조회... SSO와 회의후 세션값 정리시 수정 볼 것
/* 세션 선언[E] */

var EwsUtil				= function(){};
var EwsExcel			= function(){};
var EwsGrid				= function(){};
var EwsCurrentDateUtil	= function(){};
var EwsPopup			= function(){};
var EwsBtn				= function(){};
var EwsAuth				= function(){};

/**
 * selectBox에 공통코드 값을 넣어준다.
 */
EwsUtil.prototype = {
		/*
		 * 시나리오 조회 공통 변수 값 설정
		 * @param id : id
		 * @example ewsUtil.gfnSetSearch("SELECTMULTI");
		 */
		gfnSetSearch : function(path){	
			["gfnCommSearch"];
			try {
				WebSquare.ModelUtil.setInstanceValue( path + "/@page",	 currPage);	//*** 선택한 페이지 page 관련
				WebSquare.ModelUtil.setInstanceValue( path + "/@pageSize", pageSize);	//*** 선택한 페이지 page 관련
				
				WebSquare.ModelUtil.setInstanceValue( path + "/MODE/@value", "LIST" ); // 조회
				
				WebSquare.ModelUtil.setInstanceValue( path + "/ST_DT/@value", ST_DT.getValue() );	// 고정
				WebSquare.ModelUtil.setInstanceValue( path + "/ED_DT/@value", ED_DT.getValue() );	// 고정
				
				
				WebSquare.ModelUtil.setInstanceValue( path + "/EMP_ID/@value", ssUserId );		// 로그인한 사용자의 아이디
				WebSquare.ModelUtil.setInstanceValue( path + "/DEPT_ID/@value", ssDeptId );		// 로그인한 사용자의 부서아이디
				WebSquare.ModelUtil.setInstanceValue( path + "/DEPT_NM/@value", ssSmpDeptNm );	// 로그인한 사용자의 부서명
				WebSquare.ModelUtil.setInstanceValue( path + "/KRI_AUTH/@value", KRI_AUTH );	// 시나리오 권한
				
				
				WebSquare.ModelUtil.setInstanceValue( path + "/KRI_ID/@value", vKRI_ID );	// 시나리오 ID
				
				WebSquare.ModelUtil.setInstanceValue( path + "/STATUS/@value", STATUS.getValue() );	// 시나리오 상태
				
	            WebSquare.ModelUtil.setInstanceValue( path + "/@PID", 1);
	            WebSquare.ModelUtil.setInstanceValue( path + "/@task", vTask );
	            WebSquare.ModelUtil.setInstanceValue( path + "/@action", "retrieve" );
				
			} catch (e) {
				alert ("gfnCommSearch : " + e);
			}
		},
		/*
		 * 로그인시 메뉴를 조회한다.
		 * @param treeview menu : treeview object
		 * @example ewsUtil.gfnCommMenu();
		 */
		gfnCommMenu : function(val1, val2, val3){
			["gfnCommMenu"];
			try {
				var doc = WebSquare.xml.parse("<common PID='2' task='com.ews.com.service.CommonService' action='retrieve'></common>");
				var xPath = "/common";
				var http = WebSquare.core.getXMLHTTPObject();
					topYn = (val1) ? 'Y':'N';
					adminYn = (val2) ? 'Y':'N';
					
					WebSquare.xml.setValue( doc, xPath+"/SERVICE", "value", "3");
					WebSquare.xml.setValue( doc, xPath+"/EMP_ID", "value", ssUserId);
					WebSquare.xml.setValue( doc, xPath+"/TOP_YN", "value", topYn);
					WebSquare.xml.setValue( doc, xPath+"/ADMIN_YN","value", adminYn);
					WebSquare.xml.setValue( doc, xPath+"/MENU_UPID","value", val3);

				http.open('POST', collPath, false);
				//http.send(doc.xml);
				http.send(WebSquare.xml.serialize(doc));

				var resultObj = null;
				var xpath =  "response";
				if (http.status >= "400") {
					http = null;
				} else {
					resultObj = http.responseXML;
					http = null;
					if(val1){
						fnTopMenuCreate(resultObj);	// top 메뉴를 그린다.
					}else{
						fnLeftMenuNode(resultObj,val3);
						//WebSquare.ModelUtil.setInstanceNode(resultObj, xpath, null, "replace");
					}
				}
			} catch (e) {
				alert ("gfnCommMenu : " + e);
			}
		},
		
		/*
		 * 조직도를 조회한다.
		 * @param treeview : treeview object
		 * @example ewsUtil.gfnCommTreeview();
		 */
		gfnCommTreeview : function(obj){
			["gfnCommTreeview"];
			try {
				var doc = WebSquare.xml.parse("<common PID='2' task='com.ews.com.service.CommonService' action='retrieve'></common>");
				var xPath = "/common";
				var http = WebSquare.core.getXMLHTTPObject();
					WebSquare.xml.setValue( doc, xPath+"/SERVICE", "value", obj);

					http.open('POST', collPath, false);
				//http.send(doc.xml);
				http.send(WebSquare.xml.serialize(doc));

				var resultObj = null;
				var xpath = "response/common";

				if (http.status >= "400") {
					http = null;
				} else {
					
					resultObj = http.responseXML;
					http = null;

					WebSquare.ModelUtil.setInstanceNode(resultObj, xpath, null, "replace");
				}
			} catch (e) {
				alert ("gfnCommTreeview : " + e);
			}
		},
		
		/*
		 * 프로세스 메뉴 트리를 조회한다.
		 * @param treeview : treeview object
		 * @example ewsUtil.gfnProcessTreeview();
		 */
		gfnProcessTreeview : function(){
			["gfnProcessTreeview"];
	    	try {
				var doc = WebSquare.xml.parse("<common PID='5' task='com.ews.ope.service.OpermngService' action='retrieve'></common>");
				var xPath = "/common";
				var http = WebSquare.core.getXMLHTTPObject();
				
					WebSquare.xml.setValue( doc, xPath+"/MODE", "value", "PROCESS_TREE");

				http.open('POST', collPath, false);
				http.send(WebSquare.xml.serialize(doc));

				var resultObj = null;
				var xpath = "response/common";
				if (http.status >= "400") {
					http = null;
				} else {
					resultObj = http.responseXML;
					http = null;
					WebSquare.ModelUtil.setInstanceNode(resultObj, xpath, null, "replace");
				}
			} catch (e) {
				alert ("gfnProcessTreeview : " + e);
			}
	    },
	    
		/*
		 * 공통코드를 조회한다.
		 * @param selectboxObj : select object(array)
		 * @param comGroup : 코드값
		 * @param firstOption : 순서("" : -전체-, C : -선택- :, X : 코드 첫번째값, 기타 추가)
		 * @param comCodeArray : 제외시킬 코드값 array형태로 보내줌('1,2') ex)ewsUtil.gfnCommCodeSelectBox("select2", "LN021", "", "3");
		 * @example ewsUtil.gfnCommCodeSelectBox(selectobject, "code", "", codeArray);
		 * ewsUtil.gfnCommCodeSelectBox("selectbox1", "E04","X");
		 */
	    gfnCommCodeSelectBox : function(selectboxId, comGroup, firstOption, comCodeArray){
			["gfnCommCodeSelectBox"];
			try {
				if (typeof selectboxId == "undefined" || selectboxId == null || selectboxId == "") {
					ewsMsg.gfnAlert("g","code");
					return;
				}

				var comCode = "";
				var codeArray = [];
				if (typeof comCodeArray == "undefined" || comCodeArray == null || comCodeArray == "") {
				}else{
					codeArray = comCodeArray.split(",");
					for(var i=0; i<codeArray.length; i++){
						if(i!=0) comCode +=",";
						comCode += codeArray[i].trim().quote();
						
					}
					// comCode = comCode.substring(comCode.indexOf("'") + 1);
					// comCode = comCode.substring(0, comCode.lastIndexOf("'"));
				}
				
				var doc = WebSquare.xml.parse("<common PID='4' task='com.ews.com.service.CommonService' action='retrieve'></common>");
				var xPath = "/common";
				var http = WebSquare.core.getXMLHTTPObject();
				WebSquare.xml.setValue( doc, xPath+"/COM_GROUP", "value", comGroup);
				WebSquare.xml.setValue( doc, xPath+"/COM_CODE", "value", comCode);
				
				http.open('POST', collPath, false);
				//alert(WebSquare.xml.serialize(doc));
				//http.send(doc.xml);
				http.send(WebSquare.xml.serialize(doc));
				
				var resultObj = null;
				if (http.status >= "400") {
					http = null;
				} else {
					
					resultObj = http.responseXML;
					http = null;
					//alert(resultObj.xml);
					//$l(resultObj.xml);

					/* SELECTBOX에 값 추가 */
					var list = resultObj.getElementsByTagName("data");
					var len = list.length;

					var xPathEle = "RESULT/";
					//$l("len" + len);
					//window[selectboxId].deleteItem(idx);
					//alert(list);
					var selectboxArray = selectboxId.split(","); 
					for(var j=0; j<selectboxArray.length; j++){
						window[selectboxArray[j]].removeAll();
						if (firstOption == null || firstOption == "") {
							window[selectboxArray[j]].addItem("", "-전체-");
						} else if (firstOption.trim() == "C"){
							window[selectboxArray[j]].addItem("", "-선택-");
						} else if (firstOption.trim() != "X"){
						   window[selectboxArray[j]].addItem("", firstOption);
						}
						
						for (var i=0; i<len; i++){
							var cd   = WebSquare.xml.getValue(list[i], xPathEle+"VALUE"  , "value");
							var cdNm = WebSquare.xml.getValue(list[i], xPathEle+"LABEL" , "value");
							var find = false;
							for(var k=0;k<codeArray.length;k++){
								if(cd == codeArray[k]){
									find = true;
									break;
								}
							}
							if(!find)
								window[selectboxArray[j]].addItem(cd, cdNm);
						}
						window[selectboxArray[j]].setSelectedIndex(0);
					}
				}
			} catch (e) {
				alert ("gfnCommCodeSelectBox : " + e);
			}
		},

		/*
		 * 공통코드를 조회한다.
		 * @param comGroup : 코드값
		 * @param firstOption : 순서("" : -전체-, C : -선택- :, X : 코드 첫번째값, 기타 추가)
		 * @param comCodeArray : 제외시킬 코드값 array형태로 보내줌('1,2') ex)ewsUtil.gfnCommCodeSelectBox("select2", "LN021", "", "3");
		 * @example ewsUtil.gfnCommCodeGrid("code", "", codeArray);
		 * ewsUtil.gfnCommCodeGrid("E04","X");
		 * response/E04/vector/data/RESULT
		 * LABEL/@value
		 * VALUE/@value
		 */
		 gfnCommCodeGrid : function(comGroup, firstOption, comCodeArray){
			["gfnCommCodeGrid"];
			try {
				if (typeof comGroup == "undefined" || comGroup == null || comGroup == "") {
					ewsMsg.gfnAlert("g","code");
					return;
				}
				
				var comCode = "";
				if (typeof comCodeArray == "undefined" || comCodeArray == null || comCodeArray == "") {
				}else{
					var codeArray = comCodeArray.split(",");
					for(var j=0; j<codeArray.length; j++){
						if(j!=0) comCode +=",";
						comCode += codeArray[j].trim().quote();
					}
				}
				
				
	
				var doc = WebSquare.xml.parse("<common PID='4' task='com.ews.com.service.CommonService' action='retrieve'></common>");
				var xPath = "/common";
				var http = WebSquare.core.getXMLHTTPObject();
				WebSquare.xml.setValue( doc, xPath+"/COM_GROUP", "value", comGroup);
				WebSquare.xml.setValue( doc, xPath+"/COM_CODE", "value", comCode);
	
				http.open('POST', collPath, false);
				http.send(WebSquare.xml.serialize(doc));
	
				var resultObj = null;
				var xpath = "response/"+comGroup;
				if (http.status >= "400") {
					http = null;
				} else {
					resultObj = http.responseXML;
					http = null;

					if (firstOption == null || firstOption == "") {
						WebSquare.ModelUtil.setInstanceValue(xpath+"/vector/data/RESULT/VALUE/@value", "" );
						WebSquare.ModelUtil.setInstanceValue(xpath+"/vector/data/RESULT/LABEL/@value", "-전체-");
					} else if (firstOption.trim() == "C"){
						WebSquare.ModelUtil.setInstanceValue(xpath+"/vector/data/RESULT/VALUE/@value", "" );
						WebSquare.ModelUtil.setInstanceValue(xpath+"/vector/data/RESULT/LABEL/@value", "-선택-");
					} else if (firstOption.trim() != "X"){
						WebSquare.ModelUtil.setInstanceValue(xpath+"/vector/data/RESULT/VALUE/@value", "" );
						WebSquare.ModelUtil.setInstanceValue(xpath+"/vector/data/RESULT/LABEL/@value", firstOption);
					}
					WebSquare.ModelUtil.setInstanceNode(resultObj, xpath, null, "replace");
				}
			} catch (e) {
				alert ("gfnCommCodeGrid : " + e);
			}
		},
		
		/*
		 * pageList 초기화
		 * @return true
		 */
		gfnPageInit : function(){
			["ewsUtil.gfnPageInit"];
			try {
				var pageListAry = WebSquare.WebSquareDocument.getElementsByTagName("pageList");
				for(var i=0; i<pageListAry.length; i++){
					pageListAry[i].setCount(1);
					pageListAry[i].setSelectedIndex(1);
				}
				return true;
			} catch (e) {
				alert ("ewsUtil.gfnPageInit : " + e);
			}
		},
		
		/*
		 * gfnFileDown 첨부파일 다운로드
		 * @return true
		 */
		gfnFileDown : function(xmlData){
			["ewsUtil.gfnFileDown"];
			try{
				var myDoc = WebSquare.xml.parse( xmlData);
				var flRname = WebSquare.xml.getValue(myDoc,"data/RESULT/FL_RNAME", "value");
				WebSquare.xml.setValue( myDoc, "data/RESULT/FL_RNAME", "value", encodeURIComponent(flRname) );
				var xmlDoc = WebSquare.xml.serialize( myDoc );
				WebSquare.net.download( fileDownPath, xmlDoc, "post");
			}catch(e){
				alert("ewsUtil.gfnFileDown : " + e);
			}
		},
		
		
		/*
		 * gfnFileDelete2 자료관리 게시판 첨부파일 삭제
		 * @return true
		 */
		gfnFileDelete2 : function(xmlData){
			["ewsUtil.gfnFileDelete2"];
			try{
				
				if (typeof xmlData == "undefined" || xmlData == null || xmlData == "") {
					ewsMsg.gfnAlert("g","noFileDelete");
					return;
				}
				var myDoc = WebSquare.xml.parse( xmlData );
				var flRname = WebSquare.xml.getValue(myDoc,"data/RESULT/FL_RNAME", "value");

				if(!ewsMsg.gfnConfirm("q", "other", flRname+" 파일을 삭제")) return false;
				
				var doc = WebSquare.xml.parse("<FILEDELETE PID='19' task='com.ews.ope.service.OpermngService' action='delete'></FILEDELETE>");
				var xPath = "/FILEDELETE";
				var http = WebSquare.core.getXMLHTTPObject();
				
				WebSquare.xml.setValue( doc, xPath+"/FL_ID", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_ID", "value"));
				WebSquare.xml.setValue( doc, xPath+"/FL_SEQ", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_SEQ", "value"));
				WebSquare.xml.setValue( doc, xPath+"/FL_PATH", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_PATH", "value"));
				WebSquare.xml.setValue( doc, xPath+"/FL_SNAME", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_SNAME", "value"));
				WebSquare.xml.setValue( doc, xPath+"/FL_TYPE", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_TYPE", "value"));
	
				http.open('POST', collPath, false);
				http.send(WebSquare.xml.serialize(doc));
	
				var resultObj = null;
				var xpath = "response/FILEDELETE";
				if (http.status >= "400") {
					http = null;
				} else {
					resultObj = http.responseXML;
					http = null;
					var result = WebSquare.xml.getValue(resultObj, "xdaresult" , "result");
					if(result>0){
						ewsMsg.gfnAlert("i", "delete");
						return true;
					}
				}
			}catch(e){
				alert("ewsUtil.gfnFileDelete2 : " + e);
			}
		},
		
		/*
		 * gfnFileDelete3 게시판 첨부파일 삭제
		 * @return true
		 */
		gfnFileDelete3 : function(xmlData){
			["ewsUtil.gfnFileDelete3"];
			try{
				
				if (typeof xmlData == "undefined" || xmlData == null || xmlData == "") {
					ewsMsg.gfnAlert("g","noFileDelete");
					return;
				}
				var myDoc = WebSquare.xml.parse( xmlData );
				var flRname = WebSquare.xml.getValue(myDoc,"data/RESULT/FL_RNAME", "value");

				if(!ewsMsg.gfnConfirm("q", "other", flRname+" 파일을 삭제")) return false;
				
				var doc = WebSquare.xml.parse("<FILEDELETE PID='6' task='com.ews.sta.service.StatInfoService' action='delete'></FILEDELETE>");
				var xPath = "/FILEDELETE";
				var http = WebSquare.core.getXMLHTTPObject();
				
				
				WebSquare.xml.setValue( doc, xPath+"/FL_ID", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_ID", "value"));
				WebSquare.xml.setValue( doc, xPath+"/FL_SEQ", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_SEQ", "value"));
				WebSquare.xml.setValue( doc, xPath+"/FL_PATH", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_PATH", "value"));
				WebSquare.xml.setValue( doc, xPath+"/FL_SNAME", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_SNAME", "value"));
				WebSquare.xml.setValue( doc, xPath+"/FL_TYPE", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_TYPE", "value"));
	
				http.open('POST', collPath, false);
				http.send(WebSquare.xml.serialize(doc));
	
				var resultObj = null;
				var xpath = "response/FILEDELETE";
				if (http.status >= "400") {
					http = null;
				} else {
					resultObj = http.responseXML;
					http = null;
					var result = WebSquare.xml.getValue(resultObj, "xdaresult" , "result");
					if(result>0){
						ewsMsg.gfnAlert("i", "delete");
						return true;
					}
				}
			}catch(e){
				alert("ewsUtil.gfnFileDelete3 : " + e);
			}
		},
		
		/*
		 * gfnFileDelete 진단문서 작성 첨부파일 삭제
		 * @return true
		 */
		gfnFileDelete : function(xmlData){
			["ewsUtil.gfnFileDelete"];
			try{
				
				
				if (typeof xmlData == "undefined" || xmlData == null || xmlData == "") {
					ewsMsg.gfnAlert("g","noFileDelete");
					return;
				}
				var myDoc = WebSquare.xml.parse( xmlData);
				var flRname = WebSquare.xml.getValue(myDoc,"data/RESULT/FL_RNAME", "value");

				if(!ewsMsg.gfnConfirm("q", "other", flRname+" 파일을 삭제")) return false;
				
				var doc = WebSquare.xml.parse("<FILEDELETE PID='11' task='com.ews.com.service.CommonService' action='delete'></FILEDELETE>");
				var xPath = "/FILEDELETE";
				var http = WebSquare.core.getXMLHTTPObject();
				
				WebSquare.xml.setValue( doc, xPath+"/FL_ID", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_ID", "value"));
				WebSquare.xml.setValue( doc, xPath+"/FL_SEQ", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_SEQ", "value"));
				WebSquare.xml.setValue( doc, xPath+"/FL_GB", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_GB", "value"));
				WebSquare.xml.setValue( doc, xPath+"/FL_PATH", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_PATH", "value"));
				WebSquare.xml.setValue( doc, xPath+"/FL_SNAME", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_SNAME", "value"));
				WebSquare.xml.setValue( doc, xPath+"/FL_TYPE", "value", WebSquare.xml.getValue(myDoc,"data/RESULT/FL_TYPE", "value"));
	
				http.open('POST', collPath, false);
				http.send(WebSquare.xml.serialize(doc));
	
				var resultObj = null;
				var xpath = "response/FILEDELETE";
				if (http.status >= "400") {
					http = null;
				} else {
					resultObj = http.responseXML;
					http = null;
					var result = WebSquare.xml.getValue(resultObj, "xdaresult" , "result");
					if(result>0){
						ewsMsg.gfnAlert("i", "delete");
						return true;
					}
				}
			}catch(e){
				alert("ewsUtil.gfnFileDelete : " + e);
			}
		}
};


/**
 * 엑셀저장 및 다운로드.(기타 필요옵션은 추가해서 사용할것)
 */
EwsExcel.prototype = {
	/*
	 * grid의 엑셀을 저장.
	 * @example ewsExcel.gfnExcelDown(gridObject, fileNm);
	 */
	gfnExcelDown : function(gridId, excel_filename, removeCol){
		["gfnExcelDown"];
		try {
			//$l("==="+removeCol);
			var infoArr = [];							// 그리드에 대한 내용을 추가로 다른 셀에 표현하는 경우 사용하는 배열입니다.
			var options = {};
			options.fileName = excel_filename+".xls"; 	// 파일의 이름을 결정합니다.
			options.sheetName = "sheet";
			options.type  = "1";						// type이 1인 경우 눈에 보이는 데이터를 0인 경우 실제 데이터를 가지고 옵니다.
			options.startRowIndex = 1; 					// excel에서 gird의 데이터가 시작될 row의 index입니다.
			options.startColumnIndex = 1; 				// excel에서 gird의 데이터가 시작될 column의 index입니다.
			options.removeColumns = removeCol;
			//options.foldColumns = "4";
			//options.headerColor = "BRIGHT_GREEN";
			//options.bodyColor = "YELLOW";
			//options.footerColor = "TURQUOISE";
			//options.subTotalColor = "DARK_RED";
			window[gridId].advancedExcelDownload(options,null);
		} catch (e) {
			alert ("gfnExcelDown : " + e);
		}
	},
	
	/*
	 * grid의 엑셀을 저장.
	 * @example ewsExcel.gfnExcelUp(gridObject);
	 */
	gfnExcelUp : function(gridId){
		["gfnExcelUp"];
		try {
			var options = {};
				options.headerExist  = 1; //헤더의 존재 여부 입니다.
				options.startColumnIndex = 1; //excel파일에서 gird의 데이터가 시작되 row의 index입니다..(헤더포함)
				options.startRowIndex = 1; //excel파일에서 gird의 데이터가 시작되는 column의 index입니다.(헤더 포함)
				options.sheetNo  = 0;	//excel의 sheet번호입니다.
				options.append  = 0; //append 여부입니다. 0이면 append하지 않고 새로 쓰고 1이면 그리드의 뒤쪽에 데이터를 추가로 붙여줍니다.
				options.hidden ="1";  //1이면 그리드에서 엑셀 다운로드시에 hidden을 포함했다는 의미입니다. 즉 upload시에 그리드의 hidden Column에 값을 는다는 의미입니다.
				options.fileName = "";
				window[gridId].advancedExcelUpload(options);
				window[gridId].setProperty("excel_yn", "y");
		} catch (e) {
			alert ("gfnExcelUp : " + e);
		}
	},
	
	/*
	 * grid의 엑셀을 저장.
	 * @example ewsExcel.gfnExcelDownLoad();
	 */
	gfnExcelDownLoad : function(val){
		["gfnExcelDown"];
		try {
			var grid = WebSquare.WebSquareDocument.getElementsByTagName("grid");
//			for(var i=0; i<1; i++ ){
			for(var i=0; i<grid.length; i++ ){
				var fileUrl = location.href;
				var excelNm = ewsCurrentDateUtil.gfnGetFormatDate("yyyyMMdd")+"_"+fileUrl.getFileNm();
				var removeCol = "";
				var colCnt = grid[i].getColumnCount();
				for(var j=0; j<colCnt; j++){
					if(grid[i].getColumnIndex("CHK")==j || grid[i].getColumnIndex("E_STATUS")==j || grid[i].getColumnIndex("O_STATUS")==j || grid[i].getColumnIndex("A_STATUS")==j){
						removeCol = removeCol+j+",";
					}else{
						var visible = grid[i].getColumnVisible( j );
						
						if( !visible ){
							removeCol = removeCol+j+",";
						}
					}
				}
				if(removeCol.length>1){
					removeCol = removeCol.substring(0,removeCol.length-1);
				}
				if(isNotNull(val) || val==null){
					if(grid.length==2){
						if(i==1){
							ewsExcel.gfnExcelDown(grid[i].getID(), excelNm, removeCol);
						}
					}else{
						ewsExcel.gfnExcelDown(grid[i].getID(), excelNm, removeCol);
					}
				}else{
					if(i==val){
						ewsExcel.gfnExcelDown(grid[i].getID(), excelNm, removeCol);
					}
				}
			}
		} catch (e) {
			alert ("gfnExcelDownLoad : " + e);
		}
	},
	
	/*
	 * grid의 엑셀을 저장.
	 * @example ewsExcel.gfnExcelUpLoad();
	 */
	gfnExcelUpLoad : function(){
		["gfnExcelUpLoad"];
		try {
			var grid = WebSquare.WebSquareDocument.getElementsByTagName("grid");
			for(var i=0; i<1; i++ ){
//			for(var i=0; i<grid.length; i++ ){
				ewsExcel.gfnExcelUp(grid[i].getID());
			}
		} catch (e) {
			alert ("gfnExcelUpLoad : " + e);
		}
	}
	
};


/**
 * 그리드 추가/삭제/정리
 */
EwsGrid.prototype = {
	/*
	 * grid의 row 추가
	 * 체크할 키값이 없을시 계속증가, key값이 있을시에만 증가.
	 * @example lnGrid.gfnGridAddRow(gridId,key);
	 */
	gfnGridAddRow : function(gridId,strKey){
		["gfnGridAddRow"];
		try {
			var gridObj = window[gridId];
				strKey += "";
			var gridCnt = gridObj.getRowCount();
			if( !strKey.isEmpty() ){
				ewsGrid.gfnGridDataChk(gridId,strKey);
			}
			gridObj.insertRow(0);
		} catch (e) {
			alert ("gfnGridAddRow : " + e);
		}
	},
	
	/*
	 * grid의 row 삭제
	 * @example lnGrid.gfnGridDeleteRow(gridObject,row);
	 */
	gfnGridDeleteRow : function(gridId, gridRow){
		["gfnGridDeleteRow"];
			try {
				var gridObj = window[gridId];
				var gridState = gridObj.getRowStatusValue( gridRow );
				if( isBlank(gridState) ){
					ewsMsg.gfnAlert("i","processNo", "삭제");
					return false;
				}

				if(gridState == 2 ){	// 초기,갱신상태시
					gridObj.removeRow( gridRow );
				}else{
					gridObj.deleteRow(gridRow);
				}
			} catch (e) {
				alert ("gfnGridDeleteRow : " + e);
			}
		},
	
	/*
	 * grid의 row 정리
	 * @example lnGrid.gfnGridDataChk(gridId,key);
	 */
	gfnGridDataChk : function(gridId,strKey){
		["gfnGridDataChk"];
		try {
			var gridObj = window[gridId];
			var strFild = strKey.trim();
				strFild = strFild.split(",");
			for(var i=0; i<strFild.length; i++) {
				var gridArry = gridObj.getMatchedIndex( gridObj.getColumnIndex( strFild[i] ) , "" );
				if( gridArry!="" ){
					gridObj.removeRows( gridArry );
				}
			}
		} catch (e) {
			alert ("gfnGridDataChk : " + e);
		}
	},
	
	gfnGridRowChk : function(gridId){
		["gfnGridRowChk"];
		try {
			var gridObj = window[gridId];
			var gridCnt = gridObj.getRowCount();

			if( gridCnt<=0 ) {
				ewsMsg.gfnAlert("i", "processNo", "저장");
				return false;
			}

			var gridArry = gridObj.getModifiedData();
			if( gridArry == "" ) {
				ewsMsg.gfnAlert("i", "updateNo");
				return false;
			}

			return true;
		} catch (e) {
			alert ("gfnGridRowChk: " + e);
		}
	}
	
};


/**
 * 날짜 관련 함수
 */
EwsCurrentDateUtil.prototype = {
	/*
	 * 시스템 날짜를 조회한다.
	 * @return yyyy-mm-dd hh:mm:ss.fffffffff
	 * @example ewsCurrentDateUtil.gfnSystemDate();
	 */
	gfnSystemDate : function(){
		["gfnSystemDate"];
		try {
			var doc = WebSquare.xml.parse("<SYSDATE PID='6' task='com.ews.com.service.CommonService' action='retrieve'></SYSDATE>");
			var xPath = "/Common";
			var http = WebSquare.core.getXMLHTTPObject();
			http.open('POST', collPath, false);
			http.send(WebSquare.xml.serialize(doc));
			
			var resultObj = null;
			if (http.status >= "400") {
				http = null;
			} else {
				resultObj = http.responseXML;
				http = null;
				//$l(resultObj.xml);
				return WebSquare.xml.getValue(resultObj, "RESULT/SYSDATE" , "value");
			}
		} catch (e) {
			alert ("ewsCurrentDateUtil.gfnSystemDate : " + e);
		}
	},
	
	/*
	 * 조회된 시스템 날짜를 date 형식으로 변환한다.
	 * @return date
	 * @example ewsCurrentDateUtil.gfnGetDate();
	 */
	gfnGetDate : function(){
		["gfnGetDate"];
		try {
			var getSystemDate = ewsCurrentDateUtil.gfnSystemDate();
			var dateArry = getSystemDate.split(" ");
			var ymd = dateArry[0];
			var hms = dateArry[1];
			var ymdAy = ymd.split("-");
			var hmsAy = hms.split(":");
			var date = new Date(ymdAy[0],(ymdAy[1]-1),ymdAy[2],hmsAy[0],hmsAy[1],hmsAy[2]);
			return date;
		} catch (e) {
			alert ("ewsCurrentDateUtil.gfnGetDate : " + e);
		}
	},
	
	/*
	 * 날짜를 포멧에 마춰 리턴해준다.
	 * @return date
	 * @example ewsCurrentDateUtil.gfnGetFormatDate();
	 */
	gfnGetFormatDate : function(format){
		["gfnGetFormatDate"];
		try {
			var date = ewsCurrentDateUtil.gfnGetDate();
			return WebSquare.date.getFormattedDate( date, format);
		} catch (e) {
			alert ("ewsCurrentDateUtil.gfnGetFormatDate : " + e);
		}
	},
	
	
	/*
	 * 해당월의 시작일자
	 * @return date
	 * @example ewsCurrentDateUtil.gfnGetFormatDate();
	 */
	gfnGetFirstDate : function(format){
		["gfnGetFirstDate"];
		try {
			var date = ewsCurrentDateUtil.gfnGetDate();
			var yyyyMM = WebSquare.date.getFormattedDate( date, "yyyyMM");
			return yyyyMM + "01";
		} catch (e) {
			alert ("ewsCurrentDateUtil.gfnGetFirstDate : " + e);
		}
	},
	
	/*
	 * 해당월의 종료일자
	 * @return date
	 * @example ewsCurrentDateUtil.gfnGetFormatDate();
	 */
	gfnGetLastDate : function(format){
		["gfnGetLastDate"];
		try {
			var date = ewsCurrentDateUtil.gfnGetDate();
			var yyyy = WebSquare.date.getFormattedDate( date, "yyyy");
			var MM = WebSquare.date.getFormattedDate( date, "MM");
			var year = yyyy;
			var month = MM;
			var lastday = new Date(year,month,0).getDate();
			return yyyy + MM + lastday;
		} catch (e) {
			alert ("ewsCurrentDateUtil.gfnGetLastDate : " + e);
		}
	},

	/*
	 * 시스템 요일을 리턴해준다..
	 * @return 일월화수목금토
	 * @example ewsCurrentDateUtil.gfnGetDay();
	 */
	gfnGetWeek : function(){
		["gfnGetWeek"];
		try {
			return WebSquare.date.getDay( ewsCurrentDateUtil.gfnGetFormatDate("yyyyMMdd") );
		} catch (e) {
			alert ("ewsCurrentDateUtil.gfnGetWeek : " + e);
		}
	}
};

/**
 *  날짜 포멧(2010-10-29 00:00:00.0)
 *  yyyy-MM-dd
 */
function gfnFormatDate(date){
	return date.substring(0,10);
}

/**
 * 팝업창 관련
 * @return
 */
EwsPopup.prototype = {
		/*
		 * 공통 확인창
		 * @param msgBigTyp 타입1
		 * @param msgMidTyp 타입2
		 * @param hashParam 치환할 파라미터
		 * @return
		 */
		gfnClosePopup : function(){
			["ewsPopup.gfnClosePopup"];
			try {
				WebSquare.core.closePopupWindow();
				//WebSquare.uiplugin.popup.callClose();
				return true;
			} catch (e) {
				alert ("ewsPopup.gfnClosePopup : " + e);
			}
		},
		
		/*
		 * 팝업창을 호출
		 * @return true
		 * @example ewsPopup.gfnCreatePopUp(index,param);
		 */
		gfnCreatePopUp : function(index, param){
			["ewsPopup.gfnCreatePopUp"];
			try {
				var popId 		= ""; 
				var popType 	= "";
				var popWidth	= ""; 
				var popHeight	= "";
				var popUrl		= "";
				var optStyle 	= "";
				var modal 		= "";
				
				switch (index){
					case 0:	// 조직도 팝업
						popId 	= "Popup_01"; 
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_01.xml&POP_ID="+popId;
						popWidth = 825;
						popHeight = 548;
						// optStyle= "width:820px;height:540px;";
						modal = true;
						break;
	
					case 1:	// 재무 상세조회
						popId 	= "Popup_02"; 
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_02.xml&POP_ID="+popId;
						popWidth = 825;
						popHeight = 548;
						//optStyle= "width:820px;height:540px;";
						modal = true;			
						break;
					
					case 2:	// 진단정보승인 코멘트 입력
						popId 	= "Popup_03"; 
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_03.xml&POP_ID="+popId;
						popWidth = 454;
						popHeight = 270;
						//optStyle= "width:454px;height:270px;";
						modal = true;			
						break;
					case 3:	// 실무부서용 진단
						popId 	= "Popup_04"; 
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_04.xml&POP_ID="+popId+"&AUTH="+KRI_AUTH;
						popWidth = 825;
						popHeight = 610;
						// optStyle= "width:820px;height:540px;";
						modal = true;			
						break;
					case 4:	// 시나리오 마스터 팝업
						popId 	= "Popup_05";
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_05.xml&POP_ID="+popId;
						popWidth = 655;
						popHeight = 496;
						//optStyle= "width:655px;height:496px;";
						modal = true;
						break;
					case 5:	// 시나리오 마스터 팝업
						popId 	= "Popup_06";
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_06.xml&POP_ID="+popId;
						popWidth = 581;
						popHeight = 546;
						//optStyle= "width:655px;height:496px;";
						modal = true;
						break;
					case 6:	// 시나리오 마스터 팝업
						popId 	= "Popup_07";
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_07.xml&POP_ID="+popId;
						popWidth = 581;
						popHeight = 546;
						//optStyle= "width:655px;height:496px;";
						modal = true;
						break;
					case 7:	// 시나리오 마스터 팝업
						popId 	= "Popup_08";
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_08.xml&POP_ID="+popId;
						popWidth = 655;
						popHeight = 494;
						//optStyle= "width:655px;height:496px;";
						modal = true;
						break;
					case 8:	// 시나리오 마스터 팝업
						popId 	= "Popup_09";
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_09.xml&POP_ID="+popId;
						popWidth = 656;
						popHeight = 320;
						//optStyle= "width:655px;height:496px;";
						modal = true;
						break;
					case 9:	// 대쉬보드 팝업 세부조회(업무별 3개월 현황)
						popId 	= "Popup_10";
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_10.xml&POP_ID="+popId;
						popWidth = 850;
						popHeight = 500;
						modal = true;
						break;
					case 11:	// SP410 팝업(반입물량PO번호)
						popId 	= "Popup_11";
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_11.xml&POP_ID="+popId;
						popWidth = 850;
						popHeight = 380;
						modal = true;
						break;
					case 12:	// SP410 팝업(선적물량PO번호)
						popId 	= "Popup_12";
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_12.xml&POP_ID="+popId;
						popWidth = 850;
						popHeight = 380;
						modal = true;
						break;
					case 13:	// SP410 팝업(미반입 PO)
						popId 	= "Popup_13";
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_13.xml&POP_ID="+popId;
						popWidth = 850;
						popHeight = 380;
						modal = true;
						break;
					case 14:	// 자료관리 게시판
						popId 	= "Popup_14";
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_14.xml&POP_ID="+popId;
						popWidth = 820;
						popHeight = 600;
						modal = true;
						break;
					case 15:	// 게시판
						popId 	= "Popup_15";
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_15.xml&POP_ID="+popId;
						popWidth = 820;
						popHeight = 600;
						modal = true;
						break;
					case 16:	// 게시판
						popId 	= "Popup_16";
						popType = "window";
						popUrl	= "/EWS/Common/popup/Popup_16.xml&POP_ID="+popId+"&DASH_YN=Y";
						popWidth = 820;
						popHeight = 600;
						modal = true;
						break;
				}
				
				var left = (screen.width - popWidth) / 2;
				var top = (screen.height - popHeight) / 2;
				
			   	//WebSquare.util.openPopup(popUrl,{			id 	 		: popId,
				WebSquare.uiplugin.popup.openPopup(popUrl, {id 	 		: popId,
															type 		: popType, 
															width		: String(popWidth).px(),
															height		: String(popHeight).px(),
															top			: String(top).px(),
															left		: String(left).px(),
															popupName 	: "",
															modal		: modal,
															useIFrame	: false,
															style 		: optStyle,
														/*	resizable : false,
															status : "dkdkdkdkdk",
															menubar : false,
															scrollbars : false,
															title : true, */
															xml			: param,
															popupUrl	:"popup.jsp",	// websquare.jsp 지정시
															srcData		: "base",
															destData	: "parent/base"
															}
													);
				//return true;
			} catch (e) {
				alert ("ewsPopup.gfnCreatePopUp : " + e);
			}
		},
		
		/*
		 * 부모창에 데이터 SET
		 * @return true
		 * @example ewsPopup.gfnSetPopup(objj);
		 * fnSetPopup(val); <- 함수는 각 화면에서 컨트롤...
		 */
		gfnSetPopup : function(objj){
			["ewsPopup.gfnSetPopup"];
			try {
				var xPath = "response/POPDATA";
				WebSquare.ModelUtil.setInstanceNode( WebSquare.xml.parse( objj ), xPath, null, "replace" );
				fnSetPopup(WebSquare.ModelUtil.findInstanceNode( xPath ));	// 같은 화면에 fnSetPopup가 꼭 있어야 함.
				return true;
			} catch (e) {
				alert ("ewsPopup.gfnSetPopup : " + e);
			}
		},
		
		/*
		 * 자식창에 넘길 xml param 만들기
		 * @return true
		 * @example ewsPopup.gfnParamPopup(str);
		 * JSONObj = [{"DEPT_ID":""+deptId+"","EMP_NAME":""+empName+"","CHK_YN":"N"}];  부서아이디, 사용자명, 체크박스사용유무
		 */
		gfnParamPopup : function(JSONObj){
			["ewsPopup.gfnParamPopup"];
			try {
				var doc = WebSquare.xml.parse( "<RESULT/>" );
				var vec = new WebSquare.collection.Vector();
				for(var i=0; i<JSONObj.length; i++){
					for(key in JSONObj[i]){
						var value = JSONObj[i][key];
						WebSquare.xml.setValue( doc,  "RESULT/"+key, "value", value);
					}
					//vec.addElement(WebSquare.xml.serialize(doc));
					vec.addElement(doc);
				}
				return vec.toDocument();
			} catch (e) {
				alert ("ewsPopup.gfnParamPopup : " + e);
			}
		},
		
		/*
		 * 진단 팝업 호출(그리드에서 호출)
		 * @return true
		 * @example ewsPopup.gfnSecGridLink();
		 */
		gfnSecGridLink : function(obj,row,col){
			["ewsPopup.gfnSecGridLink"];
			try {
				if(obj.getColumnIndex( "E_ID" )==col){
					if( !isNotNull(obj.getCellData( row, "E_ID" )) ){
						var vEId = obj.getCellData(row,"E_ID");
						var jsonObj = [{"ID":""+vEId+"","KRI_ID":""+vKRI_ID+"","KRI_AUTH":"E","AUTH":""+KRI_AUTH+""}];	// 제이슨 형태
						ewsPopup.gfnCreatePopUp(3,ewsPopup.gfnParamPopup(jsonObj));
					}
				}
				
				if(obj.getColumnIndex( "O_ID" )==col){
					if( !isNotNull(obj.getCellData( row, "O_ID" )) ){
						var vOId = obj.getCellData(row,"O_ID");
						var jsonObj = [{"ID":""+vOId+"","KRI_ID":""+vKRI_ID+"","KRI_AUTH":"O","AUTH":""+KRI_AUTH+""}];	// 제이슨 형태
						ewsPopup.gfnCreatePopUp(3,ewsPopup.gfnParamPopup(jsonObj));
					}
				}
			} catch (e) {
				alert ("ewsPopup.gfnSecGridLink : " + e);
			}
		}
};


/**
 * 권한에 따른 그리드 버튼 컨틀롤
 * e_status : 실무
 * o_status : 주관
 * a_status : 관리
 */
function gfnScenarioLoad() {
		var gridArray = WebSquare.WebSquareDocument.getElementsByTagName("grid");
		gridArray[0].setColumnVisible( "E_ID", true );
		gridArray[0].setColumnVisible( "O_ID", true );

		if(KRI_AUTH=="E"){	// 실무
			gridArray[0].setColumnVisible( "E_STATUS", true );
			gridArray[0].setColumnVisible( "CHK", true );
			
			if(vKRI_RANK=="E0701"){
				btnSaisie.show();	// 진단자료입력
			}
			
		}else if(KRI_AUTH=="O"){	// 주관
			gridArray[0].setColumnVisible( "O_STATUS", true );
			gridArray[0].setColumnVisible( "CHK", true );
			
			if(vKRI_RANK=="E0701"){
				btnSaisie.show();	// 진단자료입력
			}else if(vKRI_RANK=="E0702"){
				btnConfirm.show();	// 확인
			}
			
		}else if(KRI_AUTH=="A"){	// 관리
			gridArray[0].setColumnVisible( "A_STATUS", true );
			gridArray[0].setColumnVisible( "CHK", true );
			
			btnConfirm.show();	// 확인
			
		}else{ 
			gridArray[0].setColumnVisible( "O_STATUS", true );
		}
		
		ewsUtil.gfnCommCodeSelectBox("STATUS", "E08", "", "");
		
		var pMonth = WebSquare.net.getParameter( "MONTH" );			// param
		var pStatus = WebSquare.net.getParameter( "STATUS" );			// param
		if(!isNotNull(pMonth)){
			ST_DT.setValue(pMonth + "01");
			var year = pMonth.substring(0,4);
			var month = pMonth.substring(4,6);
			var lastday = new Date(year,month,0).getDate();
			ED_DT.setValue(year + month + lastday);
		}else{
			ST_DT.setValue(ewsCurrentDateUtil.gfnGetFirstDate());
	    	ED_DT.setValue(ewsCurrentDateUtil.gfnGetLastDate());
		}
		
		if(!isNotNull(pStatus)){
			STATUS.setValue(pStatus);
		}else{
			STATUS.setValue("");
		}
		 
    	btnExcelDownLoad.show();
    	
    	$("#btnSearch").click();
}

/**
 * 시나리오 데이터 조회후 그리드 세팅
 */		
function gfnScenarioSubmit(){
	var gridArray = WebSquare.WebSquareDocument.getElementsByTagName("grid");
	var options = {};
	var back_ground = "#d2e49a";
	var sort = 0;
	if(KRI_AUTH=="E"){	// 실무
		for(var i=0; i<gridArray[0].getRowCount();i++){
			var eStaus = gridArray[0].getCellData( i, "E_STATUS" );
			if(eStaus=="N" || eStaus=="R"){
				gridArray[0].setHeaderDisabled( "HEAD_CHK", false );
				gridArray[0].setCellDisabled( i, "CHK", false );
				gridArray[0].setCellReadOnly( i, "CHK", false );
				gridArray[0].setRowBackgroundColor( i , back_ground );
			}else{
				gridArray[0].setHeaderDisabled( "HEAD_CHK", true );
				gridArray[0].setCellDisabled( i, "CHK", true );
				gridArray[0].setCellReadOnly( i, "CHK", true );
			}
    	}
		gridArray[0].sort( "E_STATUS", sort );
		
	}else if(KRI_AUTH=="O"){	// 주관
		for(var i=0; i<gridArray[0].getRowCount();i++){
    		var oStaus = gridArray[0].getCellData( i, "O_STATUS" );
    		var eStaus = gridArray[0].getCellData( i, "E_STATUS" );
    		if((eStaus=="C" && oStaus=="N") || (eStaus=="C" && oStaus=="R") ){
    			gridArray[0].setHeaderDisabled( "HEAD_CHK", false );
    			gridArray[0].setCellDisabled( i, "CHK", false );
    			gridArray[0].setCellReadOnly( i, "CHK", false );
    			gridArray[0].setRowBackgroundColor( i , back_ground );
    		}else{
    			gridArray[0].setHeaderDisabled( "HEAD_CHK", true );
    			gridArray[0].setCellDisabled( i, "CHK", true );
    			gridArray[0].setCellReadOnly( i, "CHK", true );
    		}
    	}
		gridArray[0].sort( "O_STATUS", sort );
		gridArray[0].sort( "E_STATUS", sort );
		
	}else if(KRI_AUTH=="A"){	// 관리
		for(var i=0; i<gridArray[0].getRowCount();i++){
			var eStaus = gridArray[0].getCellData( i, "E_STATUS" );
    		var aStaus = gridArray[0].getCellData( i, "A_STATUS" );
    		var oStaus = gridArray[0].getCellData( i, "O_STATUS" );
    		if((eStaus=="C" && oStaus=="C" && aStaus=="N") || (eStaus=="C" && oStaus=="C" && aStaus=="R")){
    			gridArray[0].setHeaderDisabled( "HEAD_CHK", false );
    			gridArray[0].setCellDisabled( i, "CHK", false );
    			gridArray[0].setCellReadOnly( i, "CHK", false );
    			gridArray[0].setRowBackgroundColor( i , back_ground );
    		}else{
    			gridArray[0].setHeaderDisabled( "HEAD_CHK", true );
    			gridArray[0].setCellDisabled( i, "CHK", true );
    			gridArray[0].setCellReadOnly( i, "CHK", true );
    		}
    	}
		gridArray[0].sort( "E_STATUS", sort );
		gridArray[0].sort( "A_STATUS", sort );
		gridArray[0].sort( "O_STATUS", sort );
		
	}else if(KRI_AUTH=="R"){ 
		for(var i=0; i<gridArray[0].getRowCount();i++){
			var eStaus = gridArray[0].getCellData( i, "E_STATUS" );
				gridArray[0].setHeaderDisabled( "HEAD_CHK", true );
				gridArray[0].setCellDisabled( i, "CHK", true );
				gridArray[0].setCellReadOnly( i, "CHK", true );
				gridArray[0].setRowBackgroundColor( i , back_ground );
    	}
	}
}

/**
 * 권한에 따른 그리드 버튼 컨틀롤
 */
EwsBtn.prototype = {
	
	/*
	 * 확인 btn click
	 * @return true
	 * @example ewsBtn.gfnConfirm();
	 */
	gfnConfirm : function(){
		["ewsBtn.gfnConfirm"];
		// 상태값 P로 바꿈
		// ews_kri_appr 에서 kri_id(시나리오 아이디)와 EMP_ID(세션 아이디)가 같은 APPR_ID를 o_APPR_ID 또는 a_APPR_ID에 업데이트 함.
		try {
			
			var http = WebSquare.core.getXMLHTTPObject();
			var str = "<common PID='12' task='com.ews.com.service.CommonService' action='update'>";
			
			var chkIndex = grid1.getCheckedIndex(0);
			if(chkIndex.length==0){
				alert("선택하세요");
				return;
			}
			//var chkIndex = [0,1]; 			
			var hash = new WebSquare.collection.Hashtable();
			var saveApprovalVector = new WebSquare.collection.Vector(); 
			for(var i=0;i<chkIndex.length;i++){
				WebSquare.ModelUtil.setInstanceValue("RESULT/CHECK_KEY/@value", grid1.getCellData(chkIndex[i], "CHECK_KEY"));  
				WebSquare.ModelUtil.setInstanceValue("RESULT/KRI_AUTH/@value", KRI_AUTH);      
				WebSquare.ModelUtil.setInstanceValue("RESULT/EMP_ID/@value", ssUserId);    
				WebSquare.ModelUtil.setInstanceValue("RESULT/STATUS/@value", "P"); 
				WebSquare.ModelUtil.setInstanceValue("RESULT/KRI_ID/@value", vKRI_ID);    
				saveDocument = WebSquare.ModelUtil.findInstanceNode("RESULT");       
			    WebSquare.ModelUtil.removeInstanceNode("RESULT");
				// Vector 생성
			    saveApprovalVector.addElement(WebSquare.xml.serialize(saveDocument));    
			}
			hash.put("DATA",saveApprovalVector);
			str += hash.toString();
            str += "</common>";
			http.open('POST', collPath, false);
			http.send(str);
			var resultObj = null;
			var xpath =  "response";
			if (http.status >= "400") {
				http = null;
			} else {
				resultObj = http.responseXML;
				http = null;
				
				alert("확인처리 되었습니다.");
				menuFind();
				//if(val1){
					//fnTopMenuCreate(resultObj);	// top 메뉴를 그린다.
				//}else{
					//fnLeftMenuNode(resultObj,val3);
					//WebSquare.ModelUtil.setInstanceNode(resultObj, xpath, null, "replace");
				//}
			}
			
			/*
			var doc = WebSquare.xml.parse("<CONFIRM PID='2' task='com.ews.sce.service.SceneExeService' action='process'></CONFIRM>");
			var xPath = "/request";
			WebSquare.ModelUtil.setInstanceNode( WebSquare.xml.serialize( doc ), xPath, null, "replace" );
			
			// submission 생성
			//WebSquare.ModelUtil.getSubmission("CONFIRM").id = "CONFIRM";
			WebSquare.ModelUtil.getSubmission("CONFIRM").ref = "request/CONFIRM";
			WebSquare.ModelUtil.getSubmission("CONFIRM").target = "response/CONFIRM";
			WebSquare.ModelUtil.getSubmission("CONFIRM").method = "post";
			WebSquare.ModelUtil.getSubmission("CONFIRM").mediatype = "application/xml";
			WebSquare.ModelUtil.getSubmission("CONFIRM").encoding = "UTF-8";
			WebSquare.ModelUtil.getSubmission("CONFIRM").instance = "";
			WebSquare.ModelUtil.getSubmission("CONFIRM").replace = "instance";
			WebSquare.ModelUtil.getSubmission("CONFIRM").errorHandler = "";
			WebSquare.ModelUtil.getSubmission("CONFIRM").customHandler = "";
			WebSquare.ModelUtil.getSubmission("CONFIRM").mode = "asynchronous";
			WebSquare.ModelUtil.getSubmission("CONFIRM").processMsg = " ";
			
			gfnExecuteSubmission("CONFIRM");
			*/
			/*
				var http = WebSquare.core.getXMLHTTPObject();
				
				//WebSquare.xml.setValue( doc, xPath+"/MENU_UPID","value", val3);

				http.open('POST', collPath, false);
				//http.send(doc.xml);
				http.send(WebSquare.xml.serialize(doc));

				var resultObj = null;
				var xpath =  "response";
				if (http.status >= "400") {
					http = null;
				} else {
					resultObj = http.responseXML;
					http = null;
					alert("1");
				}
				*/
		} catch (e) {
			alert ("ewsBtn.gfnConfirm : " + e);
		}
	},
	
	/*
	 * 진단자료입력 btn click
	 * @return true
	 * @example ewsBtn.gfnReturnProc();
	 */
	gfnSaisie : function(){
		["ewsBtn.gfnSaisie"];
		try {
			var grid = WebSquare.WebSquareDocument.getElementsByTagName("grid")[0];
			var rowCnt = grid.getRowCount();
			var jsonData = [];
			for(var i=0; i<rowCnt; i++){
				 if(grid.getCellChecked( i, "CHK" )){
				 	var key = grid.getCellData( i, grid.getColumnIndex("CHECK_KEY") );
				 	jsonData.push({"CHECK_KEY":""+key+"","KRI_AUTH":""+KRI_AUTH+"","KRI_ID":""+vKRI_ID+""});
				 }
			}
			if( grid.getCheckedIndex( "CHK" )==null || grid.getCheckedIndex( "CHK" )=="" ){
				ewsMsg.gfnAlert("g", "row", "진단자료입력");
				return false;
			}else{
				ewsPopup.gfnCreatePopUp(3,ewsPopup.gfnParamPopup(jsonData));
			}
		} catch (e) {
			alert ("ewsBtn.gfnSaisie : " + e);
		}
	}
};

/*
 * 권한 체크
 */
EwsAuth.prototype = {
		
		/*
		 * 유저 권한
		 * @return true
		 * @example ewsAuth.gfnSessionAuth();
		 */
		gfnSessionAuth : function(){
			try{
				var returnVal = "";
				if(ssAuthId=="10"){
					returnVal = "admin";
				}else if(ssAuthId=="50"){
					returnVal = "management";
				}else if(ssAuthId=="AA" || ssAuthId=="AB" || ssAuthId=="AC" || ssAuthId=="AD" || ssAuthId=="AE" || ssAuthId=="AF" ){
					returnVal = "officer";
				}else if(ssAuthId=="BA" || ssAuthId=="BB" || ssAuthId=="BC" || ssAuthId=="BD" || ssAuthId=="BE"){
					returnVal = "officer";
				}else if(ssAuthId=="20" || ssAuthId=="40"){
					returnVal = "user";
				}else{
					;
				}
				return returnVal;
			}catch (e){
				alert ("ewsAuth.gfnSessionAuth : " + e);
			}
		},
		
		/*
		 * 관리자 권한 체크
		 * @return true
		 * @example ewsAuth.gfnAdminAuth();
		 */
		gfnAdminAuth : function(){
			try{
				if( ewsAuth.gfnSessionAuth() == "admin" ) {
					return true;
				}
				return false;
			}catch (e){
				alert ("ewsAuth.gfnAdminAuth : " + e);
			}
		},
		
		/*
		 * 경영진 권한 체크
		 * @return true
		 * @example ewsAuth.gfnAdminAuth();
		 */
		gfnManagementAuth : function(){
			try{
				if( ewsAuth.gfnSessionAuth() == "management" ) {
					return true;
				}
				return false;
			}catch (e){
				alert ("ewsAuth.gfnManagementAuth : " + e);
			}
		},
		
		/*
		 * 임원진 권한 체크
		 * @return true
		 * @example ewsAuth.gfnOfficerAuth();
		 */
		gfnOfficerAuth : function(){
			try{
				if( ewsAuth.gfnSessionAuth() == "officer" ) {
					return true;
				}
				return false;
			}catch (e){
				alert ("ewsAuth.gfnOfficerAuth : " + e);
			}
		},
		
		/*
		 * 일반사용자 권한 체크
		 * @return true
		 * @example ewsAuth.gfnAdminAuth();
		 */
		gfnUserAuth : function(){
			try{
				if( ewsAuth.gfnSessionAuth() == "user" ) {
					return true;
				}
				return false;
			}catch (e){
				alert ("ewsAuth.gfnUserAuth: " + e);
			}
		}
}

/**
 * 그리드 상태 이미지 리턴
 * @return html
 * @example gfnChangeImg
 */
function gfnChangeImg(val){
	var retstr = "<div class='icon_"+val+"' style='margin:0;padding:0;'></div>";
	return retstr;
}

function gfnChangeAuthImg(val){
	var retstr = "<div class='icon_dp_"+val+"' style='margin:0;padding:0;'></div>";
	return retstr;
}

//function gfnSecViewImg(){
//	var retstr = "<div class='icon_SecView'></div>";
//	return retstr;
//}

/**
 * 팝업에서 부모창 콜할시 팝업이 밑으로 숨는거에 대한 처리를 위해 사용
 * */
function gfnPopCall(popUpId) {
	popUpId.menuFind();
}


// page 공통 사용을 위한 선언
var ewsCurrentDateUtil 	= new EwsCurrentDateUtil();
var ewsGrid 			= new EwsGrid();
var ewsExcel 			= new EwsExcel();
var ewsUtil 			= new EwsUtil();
var ewsPopup 			= new EwsPopup();
var ewsBtn 				= new EwsBtn();
var ewsAuth				= new EwsAuth();
ewsUtil.gfnPageInit();	// pagelist 콤포넌트 초기화